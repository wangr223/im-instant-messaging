package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.promeg.pinyinhelper.Pinyin;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.litepal.LitePal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GroupChatList extends AppCompatActivity {
    private Button back;
    private RelativeLayout add;
    private RecyclerView gcRV;
    private RelativeLayout create;
    private List<SingleContact> groupList = new ArrayList<>();
    private List<AFriend> fl = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat_list);
        ActionBar acbar = getSupportActionBar();
        if(acbar != null)
            acbar.hide();

        Intent intent_ = this.getIntent();
        int yes = intent_.getIntExtra("yes",0);
        String add_result = intent_.getStringExtra("res");
        if( yes == 1)
            Toast.makeText(this, add_result, Toast.LENGTH_SHORT).show();

        int yes_ = intent_.getIntExtra("yes_",0);
        String add_result_ = intent_.getStringExtra("res_");
        if( yes_ == 1)
            Toast.makeText(this, add_result_, Toast.LENGTH_SHORT).show();

        int G_delete = intent_.getIntExtra("has_delete",0);
        String G_Num = intent_.getStringExtra("G_Num");
        if (G_delete == 1){
            User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me())
                    .findFirst(User.class);
            SingleContact singleContact = LitePal
                    .where("user_id = ? and G_Number = ?", String.valueOf(user.getId()), G_Num)
                    .findFirst(SingleContact.class);

            LitePal.deleteAll(Bubble.class, "singlecontact_id = ?", String.valueOf(singleContact.getId()));
            LitePal.delete(SingleContact.class,singleContact.getId());
            Toast.makeText(this, "群" + singleContact.getName() + "注销完毕", Toast.LENGTH_SHORT).show();
        }

        Local_init();
        Web_init();
        initFriends();
        back = findViewById(R.id.gc_back);
        add = findViewById(R.id.group_space_function_add);
        create = findViewById(R.id.group_space_function_create);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("TEST","ADD");
                Intent intent = new Intent(GroupChatList.this,AddGCwithGid.class);
                startActivity(intent);
                GroupChatList.this.finish();
            }
        });
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupChatList.this,CreateGC.class);
                startActivity(intent);
                GroupChatList.this.finish();
            }
        });
        gcRV = findViewById(R.id.group_rv);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupChatList.this,MyFriend.class);
                startActivity(intent);
                GroupChatList.this.finish();
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        gcRV.setLayoutManager(layoutManager);
        AFriendAdapter adapter = new AFriendAdapter(fl);
        adapter.setOnItemClickListener(mClickListener);
        gcRV.setAdapter(adapter);

    }

    private void Local_init(){
        User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
        groupList = LitePal.where("user_id = ? and isGroup = ?", String.valueOf(user.getId()),"1").find(SingleContact.class);
        //initFriends();
    }

    private void Web_init(){
        final User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http" + Singleton.getInstance().getUrl() + "/api/group/all")
                .addHeader("Cookie",Singleton.getInstance().getHeader())
                .get()
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String tempresponse = response.body().string();
                Gson gson = new Gson();
                GroupAllRes groupAllRes = gson.fromJson(tempresponse, GroupAllRes.class);
                if (groupAllRes.getMessage() != null){
                    List<SingleContact> gList = groupList;
                    List<GroupAllResMessage> groupAllResMessages = groupAllRes.getMessage();
                    //System.out.println(groupAllResMessages.size());
                    for(GroupAllResMessage groupAllResMessage : groupAllResMessages){
                        int exist = 0;
                        for(SingleContact singleContact : gList){

                            if(singleContact.getG_number().equals(groupAllResMessage.getNumber())) {
                                ++exist;
                                break;
                            }
                        }
                        if(exist == 0){
                            SingleContact singleContact = new SingleContact();
                            singleContact.setIsGroup(1);
                            singleContact.setG_number(groupAllResMessage.getNumber());
                            singleContact.setName(groupAllResMessage.getName());
                            singleContact.setMemberIDList(groupAllResMessage.getMembers());
                            singleContact.setAnnouncement(groupAllResMessage.getAnnouncement());
                            singleContact.setOwnerid(groupAllResMessage.getOwnerid());
                            singleContact.setUser_id(user.getId());
                            singleContact.save();
                            groupList.add(singleContact);
                        }
                    }
                }
            }
        });
    }

    private void initFriends() {
        for(SingleContact someone : groupList){
            AFriend aFriend = new AFriend(someone.getHead(), someone.getName(), Pinyin.toPinyin(someone.getName().charAt(0)).toUpperCase().charAt(0));
            aFriend.setF_ID(someone.getG_number());
            fl.add(aFriend);
        }

        int len = fl.size();
        for(int i = 1 ; i < len ; i++){
            for(int j = i ; j > 0 &&
                    Pinyin.toPinyin(fl.get(j).getname().charAt(0)).toUpperCase().compareTo(Pinyin.toPinyin(fl.get(j-1).getname().charAt(0)).toUpperCase()) <= 0
                    ;
                j--){
                AFriend aFriend = new AFriend(fl.get(j).getFrd_head(),
                        fl.get(j).getname(),
                        fl.get(j).getFrd_topChar());
                aFriend.setF_ID(fl.get(j).getF_ID());
                fl.set(j,fl.get(j-1));
                fl.set(j-1,aFriend);
            }
        }
        for(int i = len - 1 ; i > 0 ; i--){
            for(int j = i ; j > 0; j--) {
                if(Pinyin.toPinyin(fl.get(j-1).getname().charAt(0)).charAt(0) == Pinyin.toPinyin(fl.get(j).getname().charAt(0)).charAt(0) ){
                    if(isbiggerBoolean(Pinyin.isChinese(fl.get(j-1).getname().charAt(0)), Pinyin.isChinese(fl.get(j).getname().charAt(0))) == 1){
                        AFriend aFriend = new AFriend(fl.get(j).getFrd_head(),
                                fl.get(j).getname(),
                                fl.get(j).getFrd_topChar());
                        aFriend.setF_ID(fl.get(j).getF_ID());
                        fl.set(j, fl.get(j - 1));
                        fl.set(j - 1, aFriend);
                    }
                    else break;
                }
            }
        }

    }

    private int isbiggerBoolean( Boolean a, Boolean b){
        if ( a && !b )
            return 1;
        return  0;
    }

    private AFriendAdapter.OnItemClickListener mClickListener = new AFriendAdapter.OnItemClickListener(){
        @Override
        public void onItemClick(View v, int position) {
            OkHttpClient okHttpClient = new OkHttpClient();
            final String singleID = fl.get(position).getF_ID();
            Log.d("TEST",singleID);
            Request request = new Request.Builder()
                    .addHeader("Cookie", Singleton.getInstance().getHeader())
                    .url("http" + Singleton.getInstance().getUrl() + "/api/group/id/" + singleID)
                    .get()
                    .build();
            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    String tempresponse = response.body().string();
                    Log.d("TEST",tempresponse);
                    Gson gson = new Gson();
                    GetAGroupResponse getAGroupResponse = gson.fromJson(tempresponse, GetAGroupResponse.class);
                    GroupAllResMessage this_group = getAGroupResponse.getMessage();
                    User tempUser = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                    int user_id = tempUser.getId();
                    List<SingleContact> tempUserFriends = LitePal.where("user_id = ? and isGroup = ?", String.valueOf(user_id),"1").find(SingleContact.class);
                    int exist = 0;
                    for(SingleContact singleContact : tempUserFriends) {
                        if (singleID.equals(singleContact.getM_id())) {
                            ++exist;
                            singleContact.setAnnouncement(this_group.getAnnouncement());
                            singleContact.setMemberIDList(this_group.getMembers());
                            singleContact.setMemberNameList(this_group.getMembersname());
                            /*singleContact.setName(getAFriendResponse.getMessage().getName());
                            singleContact.setEmail(getAFriendResponse.getMessage().getEmail());
                            singleContact.setSex(getAFriendResponse.getMessage().getGender() == 1 ? "男" : "女");
                            singleContact.setMood(getAFriendResponse.getMessage().getBio());*/
                            singleContact.update(singleContact.getId());
                            break;
                        }
                    }
                    /*if(exist == 0){
                        SingleContact singleContact = new SingleContact();
                        singleContact.setIsGroup(1);
                        singleContact.setName(this_group.getName());
                        singleContact.setMemberIDList(this_group.getMembers());
                        singleContact.setMemberNameList(this_group.getMembersname());
                        singleContact.setG_number(this_group.getNumber());
                        singleContact.setAnnouncement(this_group.getAnnouncement());
                        singleContact.setOwnerid(this_group.getOwnerid());
                        singleContact.setUser_id(user_id);
                        singleContact.save();
                    }*/
                    //Log.d("TEST",contactList.get(0).getName());
                }
            });
            Intent intent = new Intent(GroupChatList.this,GroupSpace.class);
            intent.putExtra("G_Number", fl.get(position).getF_ID());
            startActivity(intent);
        }
    };
}