package com.example.ruichatver1_0;

public class Message {
    private String msg_name;
    private String msg_time;
    private String msg_message;
    private String FID;
    private String msg_head;
    private int is_G;

    public int getIs_G() {
        return is_G;
    }

    public void setIs_G(int is_G) {
        this.is_G = is_G;
    }

    public Message(){

    }

    public void setMsg_head(String msg_head) {
        this.msg_head = msg_head;
    }

    public  Message(String head, String name, String message, String time){
        this.msg_head = head;
        this.msg_name = name;
        this.msg_message = message;
        this.msg_time = time;
    }

    public String gethead() {
        return msg_head;
    }

    public String getname() {
        return msg_name;
    }

    public void setMsg_name(String msg_name) {
        this.msg_name = msg_name;
    }

    public String getmessage(){
        return  msg_message;
    }

    public String gettime(){
        return msg_time;
    }

    public void setMsg_time(String msg_time){
        this.msg_time = msg_time;
    }

    public void setMsg_message(String string){
        this.msg_message = string;
    }

    public String getFID() {
        return FID;
    }

    public void setFID(String FID) {
        this.FID = FID;
    }
}