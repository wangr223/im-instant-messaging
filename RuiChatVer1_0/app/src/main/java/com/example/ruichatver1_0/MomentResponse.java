package com.example.ruichatver1_0;

import java.util.Dictionary;
import java.util.List;

public class MomentResponse {
    private int Code;
    private String Type;
    private List<MomentResponseMessage> Message;

    public String getType() {
        return Type;
    }

    public int getCode() {
        return Code;
    }

    public void setType(String type) {
        Type = type;
    }

    public void setCode(int code) {
        Code = code;
    }


    public List<MomentResponseMessage> getMessage() {
        return Message;
    }

    public void setMessage(List<MomentResponseMessage> message) {
        Message = message;
    }
}