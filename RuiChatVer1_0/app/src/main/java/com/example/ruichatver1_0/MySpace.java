package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;
import org.litepal.parser.LitePalAttr;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Struct;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MySpace extends AppCompatActivity {

    private Button btn;//点击按钮访问
    private Button to_myfriend;
    private Button to_mymsg;
    private Button to_individual_moment;
    private Button editor;
    private ImageView my_head;
    private TextView my_name;// = findViewById(R.id.my_space_name);
    private TextView my_sex;// = findViewById(R.id.my_space_my_sex);
    private TextView my_email;// = findViewById(R.id.my_space_email);
    private TextView my_mood;// = findViewById(R.id.my_space_mood);
    private TextView my_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_space);
        ActionBar acbar = getSupportActionBar();
        if(acbar != null) acbar.hide();

        my_name = findViewById(R.id.my_space_name);
        my_sex = findViewById(R.id.my_space_my_sex);
        my_email = findViewById(R.id.my_space_email);
        my_mood = findViewById(R.id.my_space_mood);
        my_id = findViewById(R.id.my_space_id);
        my_head = findViewById(R.id.my_space_head);

        local_init();

        Intent intent = this.getIntent();
        if( intent.getIntExtra("changed" , 0) == 1){
            Toast.makeText(this, "成功修改个人信息！", Toast.LENGTH_SHORT).show();
        }

        to_individual_moment = findViewById(R.id.to_individual_moment);
        to_myfriend = findViewById(R.id.my_friend);
        to_mymsg = findViewById(R.id.my_msg);
        btn = findViewById(R.id.to_my_moment);
        editor = findViewById(R.id.title_edit);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OkHttpClient okHttpClient = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http" + Singleton.getInstance().getUrl() + "/api/moment/all")
                        .addHeader("Cookie",Singleton.getInstance().getHeader())
                        .get()
                        .build();
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                        List<SingleContact> singleContactList = user.getSingleContactList();
                        List<Post> postList = new ArrayList<>();
                        for(SingleContact singleContact : singleContactList){
                            postList.addAll(singleContact.getPost_list());
                        }
                        postList.addAll(user.getPostList());
                        String tempresponse = response.body().string();
                        Log.d("TEST", tempresponse);
                        Gson gson = new Gson();
                        MomentResponse momentResponse = gson.fromJson(tempresponse, MomentResponse.class);
                        //System.out.println(momentResponse.getMessage().size());
                        if(momentResponse.getMessage() != null) {
                            List<MomentResponseMessage> momentResponseMessages = momentResponse.getMessage();
                            //System.out.println("OnClick Moment: Response Message Size ---> " + momentResponseMessages.size());
                            for (MomentResponseMessage momentResponseMessage : momentResponseMessages) {
                                int exist = 0;
                                for (Post post : postList) {
                                    if (post.getMid().equals(momentResponseMessage.getMid())) {
                                        ++exist;
                                        Post post_ = new Post();
                                        post_.setLikers(momentResponseMessage.getLikes());
                                        //post_.setCommentList(momentResponseMessage.getComments());
                                        LitePal.deleteAll(SingleComment.class, "post_id = ?", String.valueOf(post.getId()));
                                        List<SingleComment> singleCommentList = momentResponseMessage.getComments();
                                        for(SingleComment singleComment : singleCommentList){
                                            singleComment.setPost_id(post.getId());
                                            singleComment.save();
                                        }
                                        post_.setPoster_name(momentResponseMessage.getPublisherName());
                                        post_.setPoster_head(momentResponseMessage.getAvata());
                                        //Log.d("Avata",momentResponseMessage.getAvata());
                                        post_.update(post.getId());
                                        break;
                                    }
                                }
                                if (exist == 0) {
                                    SingleContact singleContact = new SingleContact();
                                    int ffound = 0;
                                    for (SingleContact singleContact1 : singleContactList) {
                                        if (singleContact1.getIsGroup() == 0) {
                                            if (singleContact1.getM_id().equals(momentResponseMessage.getPublisherID())) {
                                                singleContact = singleContact1;
                                                ffound = 1;
                                                break;
                                            }
                                        }
                                    }
                                    if (ffound == 0) { //这是自己的朋友圈存法
                                        Post post = new Post();
                                        post.setMid(momentResponseMessage.getMid());
                                        post.setPost_text(momentResponseMessage.getContent());
                                        post.setPoster_name(user.getName());
                                        post.setUser_id(user.getId());
                                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                                        Date date = new Date(Long.parseLong(momentResponseMessage.getPublishedTime()) * 1000);//时间戳转换为long
                                        String time = simpleDateFormat.format(date);
                                        post.setPost_time(time);
                                        List<SingleComment> singleCommentList = momentResponseMessage.getComments();
                                        for(SingleComment singleComment : singleCommentList){
                                            singleComment.setPost_id(post.getId());
                                            singleComment.save();
                                        }
                                        post.setPost_imageList(momentResponseMessage.getImage());
                                        post.setCommentList(momentResponseMessage.getComments());
                                        post.setLikers(momentResponseMessage.getLikes());
                                        post.setPoster_head(momentResponseMessage.getAvata());
                                        post.save();
                                        postList.add(post);
                                    } else {//这是好友的朋友圈存法
                                        Post post = new Post();
                                        post.setMid(momentResponseMessage.getMid());
                                        post.setPost_text(momentResponseMessage.getContent());
                                        post.setPoster_name(momentResponseMessage.getPublisherName());
                                        post.setSinglecontact_id(singleContact.getId());
                                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                                        Date date = new Date(Long.parseLong(momentResponseMessage.getPublishedTime()) * 1000);//时间戳转换为long
                                        //System.out.println(System.currentTimeMillis());
                                        String time = simpleDateFormat.format(date);
                                        post.setPost_time(time);
                                        List<SingleComment> singleCommentList = momentResponseMessage.getComments();
                                        for(SingleComment singleComment : singleCommentList){
                                            singleComment.setPost_id(post.getId());
                                            singleComment.save();
                                        }
                                        post.setCommentList(momentResponseMessage.getComments());
                                        post.setLikers(momentResponseMessage.getLikes());
                                        post.setPost_imageList(momentResponseMessage.getImage());
                                        post.setPoster_head(momentResponseMessage.getAvata());
                                        post.save();
                                        postList.add(post);
                                    }
                                }
                            }
                        }
                    }

                });
                Intent intent = new Intent(MySpace.this,Moment.class);
                startActivity(intent);
                //overridePendingTransition(0, 0);
                //MySpace.this.finish();
            }
        });

        to_myfriend.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MySpace.this,MyFriend.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                MySpace.this.finish();
            }
        });

        to_mymsg.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MySpace.this,friendList.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                MySpace.this.finish();
            }
        });

        editor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MySpace.this,EditionIndividual.class);
                startActivity(intent);
                MySpace.this.finish();
            }
        });

        to_individual_moment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                    ////////OKHttp there and update the database
                    OkHttpClient okHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url("http" + Singleton.getInstance().getUrl() + "/api/moment/all/" + Singleton.getInstance().getServer_Uid_me())
                            .addHeader("Cookie",Singleton.getInstance().getHeader())
                            .get()
                            .build();
                    okHttpClient.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        }

                        @Override
                        public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                            String tempresponse = response.body().string();
                            Log.d("TEST",tempresponse);
                            Gson gson = new Gson();
                            MomentResponse momentResponse = gson.fromJson(tempresponse, MomentResponse.class);
                            if(momentResponse.getMessage() != null){
                                List<MomentResponseMessage> momentResponseMessages = momentResponse.getMessage();
                                List<Post> postList = user.getPostList();
                                for(MomentResponseMessage momentResponseMessage : momentResponseMessages){
                                    int exist = 0;
                                    for(Post post : postList){
                                        if(post.getMid().equals(momentResponseMessage.getMid())){
                                            ++exist;
                                            Post post_ = new Post();
                                            post_.setLikers(momentResponseMessage.getLikes());
                                            //post_.setCommentList(momentResponseMessage.getComments());
                                            LitePal.deleteAll(SingleComment.class, "post_id = ?", String.valueOf(post.getId()));
                                            List<SingleComment> singleCommentList = momentResponseMessage.getComments();
                                            for(SingleComment singleComment : singleCommentList){
                                                singleComment.setPost_id(post.getId());
                                                singleComment.save();
                                            }
                                            post_.setPoster_name(momentResponseMessage.getPublisherName());
                                            post_.setPoster_head(momentResponseMessage.getAvata());
                                            post_.update(post.getId());
                                            break;
                                        }
                                    }
                                    if(exist == 0){//如果存在也需要进行更新
                                        Post post = new Post();
                                        post.setMid(momentResponseMessage.getMid());
                                        post.setPost_text(momentResponseMessage.getContent());
                                        post.setPoster_name(user.getName());
                                        post.setUser_id(user.getId());
                                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                                        Date date = new Date(Long.parseLong(momentResponseMessage.getPublishedTime()) * 1000);//时间戳转换为long
                                        //System.out.println(System.currentTimeMillis());
                                        String time = simpleDateFormat.format(date);
                                        post.setPost_time(time);
                                        List<SingleComment> singleCommentList = momentResponseMessage.getComments();
                                        for(SingleComment singleComment : singleCommentList){
                                            singleComment.setPost_id(post.getId());
                                            singleComment.save();
                                        }
                                        post.setCommentList(momentResponseMessage.getComments());
                                        post.setLikers(momentResponseMessage.getLikes());
                                        post.setPost_imageList(momentResponseMessage.getImage());
                                        post.setPoster_head(momentResponseMessage.getAvata());
                                        post.save();
                                        postList.add(post);
                                    }
                                }
                            }
                        }
                    });
                    Intent intent = new Intent(MySpace.this, MyMoment.class);
                    startActivity(intent);
                    //MySpace.this.finish();
                }
            });
    }

    public Bitmap stringToBitmap(String string) {
        // 将字符串转换成Bitmap类型
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray;
            bitmapArray = Base64.decode(string, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
                    bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public void local_init(){
        User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
        //先从数据库取出,对界面进行初始化
        my_name.setText(user.getName());
        my_sex.setText(user.getSex());
        my_email.setText(user.getEmail());
        my_mood.setText(user.getMood());
        my_id.setText(user.getUid());
        my_head.setImageBitmap(stringToBitmap(user.getHead()));
        //再从服务器读取，再次初始化界面
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http" + Singleton.getInstance().getUrl() + "/api/user/self")
                .addHeader("Cookie",Singleton.getInstance().getHeader())
                .get()
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                //Log.d("TEST","Have Response");
                String tempresponse =
                        response.body().string();
                //Log.d("TEST",tempresponse);
                Gson gson = new Gson();
                UserSelfResponse userSelfResponse = gson.fromJson(tempresponse, UserSelfResponse.class);

                User tempUser = new User();
                tempUser.setEmail(userSelfResponse.getData().getEmail());
                tempUser.setName(userSelfResponse.getData().getUserInfo().getName());
                tempUser.setMood(userSelfResponse.getData().getUserInfo().getBio());
                tempUser.setSex(userSelfResponse.getData().getUserInfo().getGender()==1?"男":"女");
                tempUser.updateAll("Uid = ?", Singleton.getInstance().getServer_Uid_me());


                User user1 = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                /*//先从数据库取出,对界面进行初始化
                my_name.setText(user1.getName());
                my_sex.setText(user1.getSex());
                my_email.setText(user1.getEmail());
                my_mood.setText(user1.getMood());*/
            }
        });
    }
}