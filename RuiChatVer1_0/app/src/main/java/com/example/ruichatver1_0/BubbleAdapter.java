package com.example.ruichatver1_0;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BubbleAdapter extends RecyclerView.Adapter<BubbleAdapter.ViewHolder> {
    private List<Bubble> bubbleList;

    static class ViewHolder extends RecyclerView.ViewHolder {


        TextView send_name;
        LinearLayout rheadlay;
        LinearLayout lheadlay;
        LinearLayout leftlay;
        LinearLayout rightlay;
        ImageView rhead;
        ImageView lhead;
        TextView lefttext;
        TextView righttext;
        TextView time;
        LinearLayout timestamp;
        ImageView limage;
        ImageView rimgae;

        public ViewHolder(View view)
        {
            super(view);
            timestamp = (LinearLayout) view.findViewById(R.id.TimeStamp);
            time = (TextView) view.findViewById(R.id.msg_time);
            rhead = (ImageView) view.findViewById(R.id.right_head);
            lhead = (ImageView) view.findViewById(R.id.left_head);
            rheadlay = (LinearLayout) view.findViewById(R.id.righthead_layout);
            lheadlay = (LinearLayout) view.findViewById(R.id.lefthead_layout);
            leftlay = (LinearLayout) view.findViewById(R.id.left_layout);
            rightlay = (LinearLayout) view.findViewById(R.id.right_layout);
            lefttext = (TextView) view.findViewById(R.id.left_text);
            righttext = (TextView) view.findViewById(R.id.right_text);
            send_name = view.findViewById(R.id.bubble_name);
            limage = view.findViewById(R.id.left_img);
            rimgae = view.findViewById(R.id.right_img);
        }
    }

    public BubbleAdapter(List<Bubble> bubbleList){
        this.bubbleList = bubbleList;
    }

    @Override
    public BubbleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bubble_item,parent,false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull BubbleAdapter.ViewHolder holder, int position) {
        String last_time = "0000年00月00日 00:00";
        if(position > 0) {
            Bubble lastBubble = bubbleList.get(position - 1);
            last_time = lastBubble.getTime();
        }
        Bubble bubble = bubbleList.get(position);
        String this_time = bubble.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 mm:ss");
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        Date dateA = null;
        Date dateB = null;
        try{
            dateA = format.parse(last_time);//字符串转Date
            dateB = format.parse(this_time);
        }catch (ParseException e){
            e.printStackTrace();
        }
        calendar1.setTime(dateA);// 设置日历
        calendar2.setTime(dateB);

        long MinuteA = calendar1.getTimeInMillis() / (100*60);
        long MinuteB = calendar2.getTimeInMillis() / (100*60);
        long betweendMinutes = MinuteB-MinuteA;
        if(position == 0 || (betweendMinutes*betweendMinutes)  >= 4 ){
            holder.timestamp.setVisibility(View.VISIBLE);
            holder.time.setText(this_time);
        }
        else {
            holder.timestamp.setVisibility(View.GONE);
        }
        if(bubble.getType() == Bubble.TYPE_RECEIVED){
            holder.lhead.setImageBitmap(stringToBitmap(bubble.getAvatar()));
            holder.lheadlay.setVisibility(View.VISIBLE);
            holder.leftlay.setVisibility(View.VISIBLE);
            holder.rightlay.setVisibility(View.GONE);
            holder.rheadlay.setVisibility(View.GONE);
            holder.send_name.setText(bubble.getOwner());
            if (holder.send_name.getText().equals(""))
                holder.send_name.setVisibility(View.GONE);
            //Log.d("TEST", "Receive Content --->["  + bubble.getContent() + "].");
            if (!bubble.getContent().equals(""))
                holder.lefttext.setText(bubble.getContent());
            else{
                holder.lefttext.setVisibility(View.GONE);
                holder.limage.setVisibility(View.VISIBLE);
                holder.limage.setImageBitmap(stringToBitmap(bubble.getImage()));
                //Log.d("TEST","Image ---> " + bubble.getImage());
            }
        }
        else if(bubble.getType() == Bubble.TYPE_SENT){
            holder.rhead.setImageBitmap(stringToBitmap(bubble.getAvatar()));
            holder.lheadlay.setVisibility(View.GONE);
            holder.leftlay.setVisibility(View.GONE);
            holder.send_name.setVisibility(View.GONE);
            holder.rightlay.setVisibility(View.VISIBLE);
            holder.rheadlay.setVisibility(View.VISIBLE);
            //Log.d("TEST", "Receive Content --->["  + bubble.getContent() + "].");
            if(bubble.getContent() != null)
                holder.righttext.setText(bubble.getContent());
            else{
                holder.righttext.setVisibility(View.GONE);
                holder.rimgae.setVisibility(View.VISIBLE);
                //System.out.println("Sender -----> " + bubble.getImage());
                holder.rimgae.setImageBitmap(stringToBitmap(bubble.getImage()));
            }
        }
    }

    public Bitmap stringToBitmap(String string) {
        // 将字符串转换成Bitmap类型
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray;
            bitmapArray = Base64.decode(string, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
                    bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    public int getItemCount() {
        return bubbleList.size();
    }
}
