package com.example.ruichatver1_0;
//////需要创建一个存放联系人的数据库，以及存放与对应联系人相关联的聊天记录的数据库
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;
import org.litepal.exceptions.DataSupportException;
import org.litepal.tablemanager.Connector;

import java.security.acl.Group;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class friendList extends AppCompatActivity {
    private List<Message> MessageList = new ArrayList<>();
    private Button to_myfriend;
    private Button to_myspace;
    private MessageAdapter adapter;
    private List<SingleContact> contactList = new ArrayList<SingleContact>();
    private RecyclerView recyclerView;


    private class ChatMessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            initContactList();
            try {
                initMessages();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            adapter = new MessageAdapter(MessageList);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    private void doRegisterReceiver(){
        ChatMessageReceiver chatMessageReceiver  = new ChatMessageReceiver();
        IntentFilter filter = new IntentFilter("com.ruichatver1_0.content");
        registerReceiver(chatMessageReceiver,filter);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);

        Connector.getDatabase();
        ActionBar acbar = getSupportActionBar();
        if(acbar != null)
            acbar.hide();
        doRegisterReceiver();
        initContactList();

        try {
            initMessages();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        recyclerView = (RecyclerView) findViewById(R.id.msg_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MessageAdapter(MessageList);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(MyItemClickListener);
        to_myfriend = (Button) findViewById(R.id.my_friend);
        to_myspace = (Button) findViewById(R.id.my_space);

        to_myfriend.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(friendList.this,MyFriend.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                friendList.this.finish();

            }
        });

        to_myspace.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(friendList.this,MySpace.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                friendList.this.finish();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent intent = new Intent(friendList.this,friendList.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
        friendList.this.finish();
    }

    private void initMessages() throws ParseException {  ////////////这里使用 ContactList 初始化数据 ///////
        System.out.println(contactList.size() +  " <----- Contacts Size");
        if(MessageList.size() == 0) {
            for (SingleContact someone : contactList) {
                if (someone.getMsg_list().size() != 0) {
                    Message message = new Message();
                    message.setMsg_head(someone.getHead());
                    message.setMsg_time(someone.getMsg_list().get(someone.getMsg_list().size() - 1).getTime());
                    message.setMsg_name(someone.getName());
                    if (someone.getMsg_list().get(someone.getMsg_list().size() - 1).getType() == 1) {
                        if (!(someone.getMsg_list().get(someone.getMsg_list().size() - 1).getContent() == null)) {
                            message.setMsg_message(someone.getMsg_list().get(someone.getMsg_list().size() - 1).getContent());
                        } else message.setMsg_message("[图片]");
                    } else {
                        if (!(someone.getMsg_list().get(someone.getMsg_list().size() - 1).getContent().equals(""))) {
                            message.setMsg_message(someone.getMsg_list().get(someone.getMsg_list().size() - 1).getContent());
                        } else message.setMsg_message("[图片]");
                    }
                    if (someone.getIsGroup() == 0) {
                        message.setFID(someone.getM_id());
                        message.setIs_G(0);
                    } else {
                        message.setFID(someone.getG_number());
                        message.setIs_G(1);
                    }
                    MessageList.add(message);
                }
            }
        }
        System.out.println(MessageList.size() + "   <----  Size");
        List<Message> messageList = MessageList;

            int len = messageList.size();
            for(int i = 1 ; i < len ; i++)
                for(int j = i ; j > 0 &&  messageList.get(j).gettime().compareTo(messageList.get(j-1).gettime()) >= 0; j--){
                    Message message = new Message(messageList.get(j).gethead(),
                            messageList.get(j).getname(),
                            messageList.get(j).getmessage(),
                            messageList.get(j).gettime());
                    message.setFID(messageList.get(j).getFID());
                    message.setIs_G(messageList.get(j).getIs_G());
                    messageList.set(j,messageList.get(j-1));
                    messageList.set(j-1,message);
                }
            MessageList = messageList;
            Date now_date = new Date(System.currentTimeMillis());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
            for(int i = 0 ; i < len ; i++){
                Message message = MessageList.get(i);
                if (message.getmessage().length() > 14 )
                    MessageList.get(i).setMsg_message(message.getmessage().substring(0,14) + "...");
                Date msg_date = simpleDateFormat.parse(MessageList.get(i).gettime());
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(msg_date);
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(now_date);
                if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)
                        && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH)
                        && calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                    SimpleDateFormat format =new SimpleDateFormat("hh:mm");
                    String time = format.format(msg_date);
                    MessageList.get(i).setMsg_time(time);
                    }
            }

    }

    private MessageAdapter.OnItemClickListener MyItemClickListener = new MessageAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View v, int position) {
            Message msg = MessageList.get(position);
            String FID = msg.getFID();
            //Log.d("TEST",FID);
            Intent intent = new Intent(friendList.this, Chatting.class);
            intent.putExtra("IsGroup", msg.getIs_G());
            intent.putExtra("FID",FID);
            startActivityForResult(intent,1);
        }
    };

    private void initContactList() {
        User user = LitePal.where("Uid = ?",Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
        contactList = user.getSingleContactList();
    }
}