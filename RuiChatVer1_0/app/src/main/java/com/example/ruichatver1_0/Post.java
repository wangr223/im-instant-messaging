package com.example.ruichatver1_0;

import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;

import java.util.ArrayList;
import java.util.List;

public class Post extends LitePalSupport {
    private int id;
    private String post_time;
    private String poster_name;
    private String poster_head;
    private List<String> post_imageList = new ArrayList<>();
    private String post_text;
    private int liked = 0;
    private String Mid;
    private List<String> Likers = new ArrayList<>();
    private List<SingleComment> commentList = new ArrayList<>();
    private SingleContact singleContact;
    private int singlecontact_id;
    private int user_id;

    public List<String> getPost_imageList() {
        return post_imageList;
    }

    public void setPost_imageList(List<String> post_imageList) {
        this.post_imageList = post_imageList;
    }

    public List<String> getLikers() {
        return Likers;
    }

    public void setLikers(List<String> likers) {
        Likers = likers;
    }

    public List<SingleComment> getCommentList() {
        //return commentList;
        return LitePal.where("post_id = ?", String.valueOf(id)).find(SingleComment.class);
    }

    public void setCommentList(List<SingleComment> commentList) {
        this.commentList = commentList;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSinglecontact_id() {
        return singlecontact_id;
    }

    public void setSinglecontact_id(int singlecontact_id) {
        this.singlecontact_id = singlecontact_id;
    }

    public String getPost_text() {
        return post_text;
    }

    public String getPost_time() {
        return post_time;
    }

    public String getPoster_name() {
        return poster_name;
    }

    public void setPost_text(String post_text) {
        this.post_text = post_text;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    public void setPoster_name(String poster_name) {
        this.poster_name = poster_name;
    }


    public void like_it(){
        liked = 1;
    }

    public void like_cancel(){
        liked =  0;
    }

    public int getLiked() {
        return liked;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getMid() {
        return Mid;
    }

    public void setMid(String mid) {
        Mid = mid;
    }

    public void setLiked(int liked) {
        this.liked = liked;
    }

    public String getPoster_head() {
        return poster_head;
    }

    public void setPoster_head(String poster_head) {
        this.poster_head = poster_head;
    }
}

