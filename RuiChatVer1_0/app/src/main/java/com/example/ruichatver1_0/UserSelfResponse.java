package com.example.ruichatver1_0;

public class UserSelfResponse {
    private int Code;
    private String State;
    private UserSelfResponseData Data;

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public UserSelfResponseData getData() {
        return Data;
    }

    public void setData(UserSelfResponseData data) {
        Data = data;
    }
}
