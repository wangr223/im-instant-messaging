package com.example.ruichatver1_0;

import org.litepal.crud.LitePalSupport;

public class Bubble extends LitePalSupport
{
    public static final int TYPE_RECEIVED = 0;
    public static final int TYPE_SENT = 1;

    private String content;
    private int type;
    private String time;
    private String Avatar = "";
    private String owner = "";
    private String Image = "";
    /*private int isImgae = 0;

    public void setIsImgae(int isImgae) {
        this.isImgae = isImgae;
    }

    public int getIsImgae() {
        return isImgae;
    }*/

    private SingleContact singleContact;
    private int singlecontact_id;

    public int getSinglecontact_id() {
        return singlecontact_id;
    }

    public void setSinglecontact_id(int singlecontact_id) {
        this.singlecontact_id = singlecontact_id;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String avatar) {
        Avatar = avatar;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public int getType(){
        return type;
    }

    public void setType(int type){
        this.type = type;
    }

    public String getTime(){
        return time;
    }

    public void setTime(String time){
        this.time = time;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
