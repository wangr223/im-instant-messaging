package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddGCwithGid extends AppCompatActivity {
    private Button cancel;
    private Button confirm;
    private EditText numberTV;
    private String res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_g_cwith_gid);
        ActionBar acbar = getSupportActionBar();
        if(acbar != null ){
            acbar.hide();
        }

        cancel = findViewById(R.id.friend_title_back);
        confirm = findViewById(R.id.AGC_confirm);
        numberTV = findViewById(R.id.AGC_number);
        res = "";

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddGCwithGid.this, MyFriend.class);
                startActivity(intent);
                AddGCwithGid.this.finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = numberTV.getText().toString();
                if (!"".equals(email)) {
                    /*MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("email", email);*/
                    OkHttpClient okHttpClient = new OkHttpClient();
                    //RequestBody body = RequestBody.Companion.create(jsonObject.toString(), JSON);
                    Request request = new Request.Builder()
                            .url("http" + Singleton.getInstance().getUrl() + "/api/group/add/" + email)
                            .addHeader("Cookie",Singleton.getInstance().getHeader())
                            //.post(body)
                            .get()
                            .build();
                    okHttpClient.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(@NotNull Call call, @NotNull IOException e) {
                            Log.e("TEST","NF OKHttpSend failed");
                        }

                        @Override
                        public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                            String tempresponse = response.body().string();
                            //Log.d("TEST" , tempresponse);
                            Gson gson = new Gson();
                            LogInResponse LoginRes = gson.fromJson(tempresponse, LogInResponse.class);
                            Log.d("TEST","ANF_Response :  " + LoginRes.getMessage());
                            if( "success".equals(LoginRes.getType())){
                                res = "添加成功，等待验证！";
                                SingleContact singleContact = new SingleContact();

                            }
                            else{
                                if("The user has join the group".equals(LoginRes.getMessage()))
                                    res = "已在该群聊中！";
                                else if("The group not exist".equals(LoginRes.getMessage()))
                                    res = "此群聊不存在！";
                            }
                            Intent intent = new Intent(AddGCwithGid.this, MyFriend.class);
                            intent.putExtra("res",res);
                            intent.putExtra("yes",1);
                            startActivity(intent);
                            AddGCwithGid.this.finish();
                        }
                    });
                }
            }
        });

    }
}