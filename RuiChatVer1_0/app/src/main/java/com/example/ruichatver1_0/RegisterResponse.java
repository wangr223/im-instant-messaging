package com.example.ruichatver1_0;

public class RegisterResponse {
    private int Code;
    private String Type;

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
