package com.example.ruichatver1_0;

import java.util.List;

public class GroupAllRes {
    private int Code;
    private String Type;
    private List<GroupAllResMessage> Message;

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public List<GroupAllResMessage> getMessage() {
        return Message;
    }

    public void setMessage(List<GroupAllResMessage> message) {
        Message = message;
    }
}
