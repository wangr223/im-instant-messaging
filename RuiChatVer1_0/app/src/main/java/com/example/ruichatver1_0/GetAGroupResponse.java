package com.example.ruichatver1_0;

public class GetAGroupResponse {
    private int Code;
    private String Type;
    private GroupAllResMessage Message;

    public GroupAllResMessage getMessage() {
        return Message;
    }

    public void setMessage(GroupAllResMessage message) {
        Message = message;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
