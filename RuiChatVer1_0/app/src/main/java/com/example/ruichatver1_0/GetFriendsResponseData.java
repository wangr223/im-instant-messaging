package com.example.ruichatver1_0;

import java.util.List;

public class GetFriendsResponseData {
    private List<String> Friends;

    public List<String> getFriends() {
        return Friends;
    }

    public void setFriends(List<String> friends) {
        Friends = friends;
    }
}
