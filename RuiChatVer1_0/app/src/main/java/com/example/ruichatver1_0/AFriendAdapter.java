package com.example.ruichatver1_0;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.Visibility;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class AFriendAdapter extends RecyclerView.Adapter<AFriendAdapter.ViewHolder>{
    private List<AFriend> mFriendList;

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView headimg;
        TextView nametxt;
        TextView indextxt;

        public ViewHolder(View view){
            super(view);
            headimg = (ImageView) view.findViewById(R.id.fhead);
            nametxt = (TextView) view.findViewById(R.id.fname);
            indextxt = (TextView) view.findViewById(R.id.findex);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClickListener.onItemClick(v,getAdapterPosition());
        }
    }

    public static OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(View v, int position);
    }

    public void setOnItemClickListener(AFriendAdapter.OnItemClickListener listener){
        this.mOnItemClickListener = listener;
    }

    public AFriendAdapter(List<AFriend> friendList){
        mFriendList = friendList;
    }

    @Override
    public AFriendAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_friend,parent,false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull AFriendAdapter.ViewHolder holder, int position) {
        AFriend this_friend = mFriendList.get(position);
        if(position == 0){
            holder.indextxt.setText(String.valueOf(this_friend.getFrd_topChar()));
        }
        else{
            AFriend last_friend = mFriendList.get(position-1);
            if( this_friend.getFrd_topChar() != last_friend.getFrd_topChar())
                holder.indextxt.setText(String.valueOf(this_friend.getFrd_topChar()));
            else holder.indextxt.setVisibility(View.GONE);
        }
        holder.nametxt.setText(this_friend.getname());
        holder.headimg.setImageBitmap(stringToBitmap(this_friend.getFrd_head()));
    }

    public Bitmap stringToBitmap(String string) {
        // 将字符串转换成Bitmap类型
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray;
            bitmapArray = Base64.decode(string, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
                    bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    public int getItemCount() {
        return mFriendList.size();
    }
}