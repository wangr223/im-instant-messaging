package com.example.ruichatver1_0;

import java.util.Dictionary;

public class LogInResponse {
    private int Code;
    private String Type;
    private String Message;

    public String getType() {
        return Type;
    }

    public int getCode() {
        return Code;
    }

    public void setType(String type) {
        Type = type;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
