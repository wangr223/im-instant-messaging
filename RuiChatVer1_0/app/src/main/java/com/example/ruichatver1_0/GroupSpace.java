package com.example.ruichatver1_0;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.security.identity.CipherSuiteNotSupportedException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import org.litepal.LitePal;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GroupSpace extends AppCompatActivity {
    private String G_Number;
    private TextView ann;
    private TextView mem;
    private TextView name;
    private Button back;
    private Button go_chat;
    private Button ann_edit;
    private Button group_delete;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_space);
        ActionBar acbar = getSupportActionBar();
        if(acbar != null)
            acbar.hide();

        ann = findViewById(R.id.group_space_announcement);
        mem = findViewById(R.id.group_space_memberNames);
        name = findViewById(R.id.group_space_name);
        back = findViewById(R.id.gc_back);
        go_chat = findViewById(R.id.group_send_msg);  ///进入 Chatting
        ann_edit = findViewById(R.id.group_ann_edit); ///另起一个编辑页面
        group_delete = findViewById(R.id.group_group_delete);////弹窗询问是否确认

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupSpace.this,GroupChatList.class);
                startActivity(intent);
                GroupSpace.this.finish();
            }
        });

        go_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupSpace.this,Chatting.class);
                intent.putExtra("FID", G_Number);
                intent.putExtra("IsGroup", 1);
                startActivity(intent);
            }
        });

        Intent this_intent = this.getIntent();
        G_Number = this_intent.getStringExtra("G_Number");
        User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me())
                .findFirst(User.class);
        SingleContact this_group = LitePal.where("user_id = ? and G_number = ?", String.valueOf(user.getId()), G_Number)
                .findFirst(SingleContact.class);

        if (!this_group.getOwnerid().equals(Singleton.getInstance().getServer_Uid_me())){
            group_delete.setVisibility(View.GONE);
            ann_edit.setVisibility(View.GONE);
        }

        ann.setText(this_group.getAnnouncement());
        String names = listToString(this_group.getMemberNameList());
        mem.setText(names);////////////////////群成员处还有问题
        name.setText(this_group.getName());

        group_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dia();
            }
        });

        ann_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupSpace.this,AnnEdit.class);
                intent.putExtra("G_Num", G_Number);
                startActivity(intent);
                GroupSpace.this.finish();
            }
        });

    }

    private void dia(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                OkHttpClient okHttpClient = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http" + Singleton.getInstance().getUrl() + "/api/group/delete/" + G_Number)
                        .addHeader("Cookie",Singleton.getInstance().getHeader())
                        .delete()
                        .build();
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        Intent intent = new Intent(GroupSpace.this, GroupChatList.class);
                        intent.putExtra("has_delete",1);
                        intent.putExtra("G_Num",G_Number);
                        startActivity(intent);
                    }
                });
                dialog.cancel();
            }
        });
        builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setTitle("警告");
        builder.setMessage("是否确认删除该群？");
        builder.show();
    }

    public static String listToString(List<String> list){

        if(list==null){
            return null;
        }

        StringBuilder result = new StringBuilder();
        boolean first = true;

        //第一个前面不拼接","
        for(String string :list) {
            if(first) {
                first=false;
            }else{
                result.append(",");
            }
            result.append(string);
        }
        return result.toString();
    }
}