package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.litepal.LitePal;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class  MyMoment extends AppCompatActivity {

    private List<Post> postList = new ArrayList<>();
    private Button poster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_moment);
        ActionBar acbar = getSupportActionBar();
        if(acbar != null)
            acbar.hide();

        poster = findViewById(R.id.post_make);
        poster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyMoment.this,MomentEdit.class);
                startActivity(intent);
            }
        });

        postList.addAll(LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class).getPostList());
        init();
        sortPosts();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.my_post);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        PostAdapter adapter = new PostAdapter(postList,this);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(LikeItemClickListener, CommentItemClickListener);

    }

    private PostAdapter.OnItemClickListener LikeItemClickListener = new PostAdapter.OnItemClickListener(){
        @Override
        public void onItemClick(View v, int position) {
            Post post = postList.get(position);
            if(post.getLiked() == 0) {
                v.setBackgroundResource(R.drawable.heart_clicked);
                postList.get(position).like_it();
            }
            else{
                v.setBackgroundResource(R.drawable.heart);
                postList.get(position).like_cancel();
            }
        }
    };

    private PostAdapter.OnItemClickListener CommentItemClickListener = new PostAdapter.OnItemClickListener(){
        @Override
        public void onItemClick(View v, int position) {
            Log.d("TEST","Comment");/////////////////////////Pop typewriting and finish for real comment;
        }
    };

    private void sortPosts(){
        List<Post> posts = postList;
        //Log.d("TEST",MessageList.get(0).get);
        //System.out.println(MessageList.size());

        int len = posts.size();
        for(int i = 1 ; i < len ; i++)
            for(int j = i ; j > 0 &&  posts.get(j).getPost_time().compareTo(posts.get(j-1).getPost_time()) >= 0; j--){
                Post post = new Post();
                post.setMid(posts.get(j).getMid());
                post.setPost_text(posts.get(j).getPost_text());
                post.setPost_time(posts.get(j).getPost_time());
                post.setPoster_name(posts.get(j).getPoster_name());
                post.setSinglecontact_id(posts.get(j).getSinglecontact_id());
                post.setUser_id(posts.get(j).getUser_id());
                post.setCommentList(posts.get(j).getCommentList());
                post.setLiked(posts.get(j).getLiked());
                post.setLikers(posts.get(j).getLikers());
                post.setId(posts.get(j).getId());
                posts.set(j,posts.get(j-1));
                posts.set(j-1,post);
            }
        postList = posts;
    }

    private void init(){
        final User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
        postList = user.getPostList();
        ////////OKHttp there and update the database
        /*OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http" + Singleton.getInstance().getUrl() + "/api/moment/all/" + user.getUid())
                .addHeader("Cookie",Singleton.getInstance().getHeader())
                .get()
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String tempresponse = response.body().string();
                //Log.d("TEST",tempresponse);
                Gson gson = new Gson();
                MomentResponse momentResponse = gson.fromJson(tempresponse, MomentResponse.class);
                if (momentResponse.getMessage() != null) {
                    List<MomentResponseMessage> momentResponseMessages = momentResponse.getMessage();
                    //System.out.println(momentResponseMessages.size());
                    List<Post> postList = user.getPostList();
                    for (MomentResponseMessage momentResponseMessage : momentResponseMessages) {
                        int exist = 0;
                        for (Post post : postList) {
                            if (post.getMid().equals(momentResponseMessage.getMid())) {
                                ++exist;
                                break;
                            }
                        }
                        if (exist == 0) {//如果存在也需要进行更新
                            Post post = new Post();
                            post.setMid(momentResponseMessage.getMid());
                            post.setPost_text(momentResponseMessage.getContent());
                            post.setPoster_name(user.getName());
                            post.setUser_id(user.getId());
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                            Date date = new Date(Long.parseLong(momentResponseMessage.getPublishedTime()) * 1000);//时间戳转换为long
                            //System.out.println(System.currentTimeMillis());
                            String time = simpleDateFormat.format(date);
                            post.setPost_time(time);
                            post.save();
                            postList.add(post);
                        }
                    }
                }
            }
        });*/
    }

}