package com.example.ruichatver1_0;

import androidx.constraintlayout.solver.LinearSystem;

import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;

import java.util.ArrayList;
import java.util.List;

public class SingleContact extends LitePalSupport {
    private int id;
    private String m_id;
    private String head;
    private String email;
    private String sex;
    private String mood;
    private int user_id;
    private String name;
    private List<Bubble> msg_list = new ArrayList<Bubble>();
    private List<Post> post_list = new ArrayList<>();
    private User user;

    //Group
    private int isGroup;
    private List<String> memberIDList;
    private List<String> memberNameList;
    private String G_number;
    private String Announcement;
    private String Ownerid;

    public List<String> getMemberNameList() {
        return memberNameList;
    }

    public void setMemberNameList(List<String> memberNameList) {
        this.memberNameList = memberNameList;
    }

    public int getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(int isGroup) {
        this.isGroup = isGroup;
    }

    public String getOwnerid() {
        return Ownerid;
    }

    public void setOwnerid(String ownerid) {
        Ownerid = ownerid;
    }

    public String getAnnouncement() {
        return Announcement;
    }

    public void setAnnouncement(String announcement) {
        Announcement = announcement;
    }

    public String getG_number() {
        return G_number;
    }

    public void setG_number(String g_number) {
        G_number = g_number;
    }

    public List<String> getMemberIDList() {
        return memberIDList;
    }

    public void setMemberIDList(List<String> memberIDList) {
        this.memberIDList = memberIDList;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public int getId() {
        return id;
    }


    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Bubble> getMsg_list(){
        return LitePal.where("singlecontact_id = ?", String.valueOf(id)).find(Bubble.class);
        //return msg_list;
    }

    public void setMsg_list(List<Bubble> msg_list){
        this.msg_list = msg_list;
    }

    public List<Post> getPost_list() {
        return LitePal.where("singlecontact_id = ?", String.valueOf(id)).find(Post.class);
        //return post_list;
    }

    public void setPost_list(List<Post> post_list) {
        this.post_list = post_list;
    }

    public String getM_id() {
        return m_id;
    }

    public void setM_id(String m_id) {
        this.m_id = m_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}

/*
需要保存在本地的数据:
    FriendList：
        已存在的聊天信息，按照最新的消息进行排序.
            Name;
            Bubble;
            Head Image;
    MyFriend：
        已存在的好友目录.
            Name;
            Head Image;
            Info; （查看慕客）
    MySpace:
        个人信息;

        Posts.
            Name;
            Head Image;
            Mood Text;
            Post Images;
            Post Time;
 */
