package com.example.ruichatver1_0;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentUris;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.service.autofill.UserData;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.litepal.LitePal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class EditionIndividual extends AppCompatActivity implements View.OnClickListener{

    private static final int CHOOSE_PHOTO = 2;
    private String imagePath;
    private Bitmap head_bitmap;
    private PopupWindow mPopWindow;
    private int head_change;
    Button set_sex;
    EditText set_name;
    EditText set_email;
    EditText set_mood;
    EditText set_password;
    Button cancel;
    private Button choose_head;
    private ImageView set_head;
    Button confirm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edition_individual);
        ActionBar acbar = getSupportActionBar();
        if(acbar != null ){
            acbar.hide();
        }
        head_change = 0;

        set_sex = findViewById(R.id.individual_edit_sex);
        set_name = findViewById(R.id.individual_edit_name);
        set_email = findViewById(R.id.individual_edit_email);
        set_mood = findViewById(R.id.individual_edit_mood);
        set_password = findViewById(R.id.individual_edit_password);
        cancel = findViewById(R.id.title_cancel);
        confirm = findViewById(R.id.title_confirm);
        choose_head = findViewById(R.id.individual_select_head);
        set_head = findViewById(R.id.individual_head_choosed);

        choose_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission();
            }
        });

        final User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
        set_sex.setText(user.getSex());
        set_email.setText(user.getEmail());
        set_mood.setText(user.getMood());
        set_name.setText(user.getName());
        set_password.setText(user.getPassword());
        set_sex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupWindow();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditionIndividual.this,MySpace.class);
                startActivity(intent);
                //overridePendingTransition(0, 0);
                EditionIndividual.this.finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User tempUser = new User();
                String email = set_email.getText().toString();
                String mood = set_mood.getText().toString();
                String name = set_name.getText().toString();
                String sex = set_sex.getText().toString();
                String password = set_password.getText().toString();
                tempUser.setEmail(email);
                tempUser.setMood(mood);
                tempUser.setName(name);
                tempUser.setSex(sex);
                tempUser.setPassword(password);
                if (head_change == 1)
                    tempUser.setHead(bitmapToString(head_bitmap));
                tempUser.updateAll("Uid = ?", Singleton.getInstance().getServer_Uid_me());

                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                UserInfo userInfo = new UserInfo();
                userInfo.setBio(mood);
                userInfo.setGender("男".equals(sex) ? 1 : 0);
                userInfo.setName(name);
                if (head_change == 1)
                    userInfo.setAvatar(bitmapToString(head_bitmap));
                UserSelfResponseData userSelfResponseData = new UserSelfResponseData();
                userSelfResponseData.setEmail(email);
                userSelfResponseData.setPassword(password);
                userSelfResponseData.setUserInfo(userInfo);
                Gson gson = new Gson();
                String UD = gson.toJson(userSelfResponseData);

                OkHttpClient okHttpClient = new OkHttpClient();
                RequestBody body = RequestBody.Companion.create(UD, JSON);
                Request request = new Request.Builder()
                        .url("http" + Singleton.getInstance().getUrl() + "/api/user/message")
                        .addHeader("Cookie",Singleton.getInstance().getHeader())
                        .put(body)
                        .build();
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        //Toast.makeText(EditionIndividual.this, "账号密码错误 ", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                        Intent intent = new Intent(EditionIndividual.this, MySpace.class);
                        intent.putExtra("changed",1);
                        startActivity(intent);
                        EditionIndividual.this.finish();
                    }
                });
            }
        });

    }

    public void showPopupWindow(){
        set_sex = findViewById(R.id.individual_edit_sex);
        View contentView = LayoutInflater.from(EditionIndividual.this).inflate(R.layout.popupwin, null);
        mPopWindow = new PopupWindow(contentView,
                set_sex.getWidth(),
                ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置各个控件的点击响应
        TextView tv1 = contentView.findViewById(R.id.pop_man);
        TextView tv2 = contentView.findViewById(R.id.pop_woman);
        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
        //显示PopupWindow
        mPopWindow.showAsDropDown(set_sex);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        set_sex = (Button) findViewById(R.id.individual_edit_sex);
        switch (id){
            case R.id.pop_man:{
                //Toast.makeText(this,"clicked computer",Toast.LENGTH_SHORT).show();
                set_sex.setText("男");
                mPopWindow.dismiss();
            }
            break;
            case R.id.pop_woman:{
                //Toast.makeText(this,"clicked financial",Toast.LENGTH_SHORT).show();
                set_sex.setText("女");
                mPopWindow.dismiss();
            }
            break;
        }
    }

    //////////////////////////////////////////////
    public String bitmapToString(Bitmap bitmap){
        //将Bitmap转换成字符串
        String string=null;
        ByteArrayOutputStream bStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,bStream);
        byte[]bytes=bStream.toByteArray();
        string= Base64.encodeToString(bytes, Base64.DEFAULT);
        return string;
    }


    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }else {
            openAlbum();
        }
    }



    private void openAlbum(){
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent,CHOOSE_PHOTO); //打开相册
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 1:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    openAlbum();
                }else {
                    Toast.makeText(this,"你拒绝了该权限", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case CHOOSE_PHOTO:
                if(resultCode == RESULT_OK){
                    //判断手机系统版本号
                    if(Build.VERSION.SDK_INT>=19){
                        //4.4及以上系统使用这个方法处理图片
                        handleImageOnKitKat(data);
                    }
                }
                break;
            default:
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void handleImageOnKitKat(Intent data){
        Uri uri = data.getData();
        if(DocumentsContract.isDocumentUri(this,uri)){
            //如果是document类型的Uri，则通过document id处理
            String docId = DocumentsContract.getDocumentId(uri);
            if("com.android.providers.media.documents".equals(uri.getAuthority())){
                String id = docId.split(":")[1];  //解析出数字格式的id
                String selection = MediaStore.Images.Media._ID+"="+id;
                imagePath = getImagePath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,selection);
            }else if("com.android.providers.downloads.documents".equals(uri.getAuthority())){
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public downloads"),Long.valueOf(docId));
                imagePath = getImagePath(contentUri,null);
            }
        }else if("content".equalsIgnoreCase(uri.getScheme())){
            //如果是file类型的Uri，直接获取图片路径即可
            imagePath = getImagePath(uri,null);
        }else if("file".equalsIgnoreCase(uri.getScheme())){
            //如果是file类型的Uri，直接获取图片路径即可
            imagePath = uri.getPath();
        }
        displayImage(imagePath); //根据图片路径显示图片
    }

    private String getImagePath(Uri uri,String selection){
        String path = null;
        //通过Uri和selection来获取真实的图片路径
        Cursor cursor = getContentResolver().query(uri,null,selection,null,null);
        if(cursor!= null){
            if(cursor.moveToFirst()){
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }

    private void displayImage(String imagePath){
        if(imagePath!=null && !imagePath.equals("")){
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            set_head.setVisibility(View.VISIBLE);
            set_head.setImageBitmap(bitmap);
            head_bitmap = bitmap;
            head_change = 1;
            //存储上次选择的图片路径，用以再次打开app设置图片
            SharedPreferences sp = getSharedPreferences("sp_img",MODE_PRIVATE);  //创建xml文件存储数据，name:创建的xml文件名
            SharedPreferences.Editor editor = sp.edit(); //获取edit()
            editor.putString("imgPath",imagePath);
            editor.apply();
        }else {
            Toast.makeText(this,"获取图片失败",Toast.LENGTH_SHORT).show();
        }
    }

}