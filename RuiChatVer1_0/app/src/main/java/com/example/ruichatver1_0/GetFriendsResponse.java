package com.example.ruichatver1_0;

public class GetFriendsResponse {
    private int Code;
    private String State;
    private GetFriendsResponseData Data;

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public GetFriendsResponseData getData() {
        return Data;
    }

    public void setData(GetFriendsResponseData data) {
        Data = data;
    }
}
