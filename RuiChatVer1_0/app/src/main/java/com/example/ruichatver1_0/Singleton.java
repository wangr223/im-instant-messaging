package com.example.ruichatver1_0;

public class Singleton{
    private static Singleton instance;
    private String header;
    private String url;
    private String Uid;
    private String server_Uid_me;
    private JWebSocketClient client;

    public String getServer_Uid_me() {
        return server_Uid_me;
    }

    public void setServer_Uid_me(String server_Uid_me) {
        this.server_Uid_me = server_Uid_me;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    private Singleton (){}
    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getUid() {
        return Uid;
    }

    public void setUid(String uid) {
        Uid = uid;
    }

    public JWebSocketClient getClient() {
        return client;
    }

    public void setClient(JWebSocketClient client) {
        this.client = client;
    }
}
