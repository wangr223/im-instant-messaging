package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Registion extends AppCompatActivity {

    private EditText ruser;
    private EditText rpwd;
    private EditText remail;
    private Button conf;
    private Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registion);

        ActionBar acbar = getSupportActionBar();
        if(acbar != null ){
            acbar.hide();
        }

        ruser = (EditText) findViewById(R.id.ruser);
        remail = (EditText) findViewById(R.id.remail);
        rpwd = (EditText) findViewById(R.id.rpwd);
        conf =  (Button) findViewById(R.id.confirm);
        back = (Button) findViewById(R.id.backwords);

        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent1 = new Intent(Registion.this, Log_in.class);
                startActivity(intent1);
                Registion.this.finish();
            }
        });

        conf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                final String tempuser = ruser.getText().toString();
                final String tempmail = remail.getText().toString();
                final String temppass = rpwd.getText().toString();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("name",tempuser);
                jsonObject.addProperty("email",tempmail);
                jsonObject.addProperty("password",temppass);
                OkHttpClient okHttpClient = new OkHttpClient();
                RequestBody body = RequestBody.Companion.create(jsonObject.toString(), JSON);
                Request request = new Request.Builder()
                        .url("http" + Singleton.getInstance().getUrl() + "/api/user/register")
                        .post(body)
                        .build();
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        //Log.d("LogIn", "Failed");
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        String tempresponse = response.body().string();
                        Gson gson = new Gson();
                        //Log.d("TEST","SUCCESS");
                        RegisterResponse registerResponse = gson.fromJson(tempresponse, RegisterResponse.class);
                        //Log.d("TEST", registerResponse.getType());
                        int res = 0;
                        if(registerResponse.getType().equals("success")){
                            //Log.d("Registion","Success Registion");
                            Intent intent1 = new Intent(Registion.this, Log_in.class);
                            res = 1;
                            intent1.putExtra("result", res);
                            startActivity(intent1);
                            Registion.this.finish();
                        }
                        else{
                            //Log.d("LogIn","Failed Log In");
                        }

                    }
                });
            }
        });
    }
}