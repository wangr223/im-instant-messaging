package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.litepal.LitePal;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Moment extends AppCompatActivity {
    private List<Post> postList = new ArrayList<>();
    private View temp;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moment);
        ActionBar acbar = getSupportActionBar();
        if(acbar != null)
            acbar.hide();
        context = this;


        initPosts();
        sortPosts();
        //Log.d("TEST","Post All Ready");
        RecyclerView recyclerView = findViewById(R.id.post_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        PostAdapter adapter = new PostAdapter(postList,this);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(LikeItemClickListener, CommentItemClickListener);
        //Log.d("TEST","Adapter All Ready");
        Button make = findViewById(R.id.post_make);
        make.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent1 = new Intent(Moment.this, MomentEdit.class);
                startActivity(intent1);
                Moment.this.finish();
            }
        });

    }

    public PostAdapter.OnItemClickListener LikeItemClickListener = new PostAdapter.OnItemClickListener(){
        @Override
        public void onItemClick(View v,int position) {
            temp = v;
            Post post = postList.get(position);
            System.out.println("Position ---> " + position);
            System.out.println("Post id : " + post.getId() + " and Post content : " + post.getPost_text());
            if(post.getLiked() == 0) {
                User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                Post post_ = new Post();
                post_.setLiked(1);
                List<String> lovers = post.getLikers();
                lovers.add(user.getName());
                System.out.println("Update User Name --> " + user.getName());
                post_.setLikers(lovers);
                post_.update(post.getId());
                postList.get(position).setLiked(1);

                //另外需要一个向服务器的点赞提醒
                temp.setBackgroundResource(R.drawable.heart_clicked);
                postList.get(position).like_it();
                OkHttpClient okHttpClient = new OkHttpClient();
                JsonObject jsonObject = new JsonObject();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                jsonObject.addProperty("momentid",post.getMid());
                final RequestBody body = RequestBody.Companion.create(jsonObject.toString(), JSON);
                Request request = new Request.Builder()
                        .url("http" + Singleton.getInstance().getUrl() + "/api/moment/like")
                        .addHeader("Cookie", Singleton.getInstance().getHeader())
                        .post(body)
                        .build();
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        /*User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                        Post post_ = new Post();
                        post_.setLiked(1);
                        List<String> lovers = post.getLikers();
                        lovers.add(user.getName());
                        System.out.println("Update User Name --> " + user.getName());
                        post_.setLikers(lovers);
                        post_.updateAll("id = ?", String.valueOf(post.getId()));*/
                    }
                });
            }
            else {
                Log.d("TEST","Liked before");
            }
        }
    };

    public PostAdapter.OnItemClickListener CommentItemClickListener = new PostAdapter.OnItemClickListener(){
        @Override
        public void onItemClick(View v, int position) {
            final Post post = postList.get(position);
            //弹出输入框并输入
            final EditText inputServer = new EditText(context);
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("写下评论")
                    //.setIcon(android.R.drawable.ic_dialog_info)
                    .setView(inputServer)
                    .setNegativeButton("取消", null);
            builder.setPositiveButton("评论", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    String content = inputServer.getText().toString();
                    SingleComment singleComment = new SingleComment();
                    singleComment.setContent(content);

                    User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                    singleComment.setPublisherName(user.getName());

                    singleComment.setPost_id(post.getId());

                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("momentid", post.getMid());
                    jsonObject.addProperty("content", content);
                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                    RequestBody body = RequestBody.Companion.create(jsonObject.toString(), JSON);
                    Request request = new Request.Builder()
                            .url("http" + Singleton.getInstance().getUrl() + "/api/moment/comment")
                            .addHeader("Cookie", Singleton.getInstance().getHeader())
                            .post(body)
                            .build();
                    OkHttpClient okHttpClient = new OkHttpClient();
                    okHttpClient.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        }

                        @Override
                        public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        }
                    });
                    dialog.cancel();
                }
            });
            builder.show();
            //OKHttp Comment请求
            //在得到回复后
        }
    };

    private void sortPosts(){
        List<Post> posts = postList;
        int len = posts.size();
        for(int i = 1 ; i < len ; i++)
            for(int j = i ; j > 0 &&  posts.get(j).getPost_time().compareTo(posts.get(j-1).getPost_time()) >= 0; j--){
                Post post = new Post();
                post.setMid(posts.get(j).getMid());
                post.setId(posts.get(j).getId());
                post.setPost_text(posts.get(j).getPost_text());
                post.setPost_time(posts.get(j).getPost_time());
                post.setPoster_name(posts.get(j).getPoster_name());
                post.setSinglecontact_id(posts.get(j).getSinglecontact_id());
                post.setLiked(posts.get(j).getLiked());
                post.setCommentList(posts.get(j).getCommentList());
                post.setLikers(posts.get(j).getLikers());
                post.setPoster_head(posts.get(j).getPoster_head());
                post.setPost_imageList(posts.get(j).getPost_imageList());
                post.setUser_id(posts.get(j).getUser_id());
                posts.set(j,posts.get(j-1));
                posts.set(j-1,post);
            }
        postList = posts;
    }

    private void initPosts() {
        //首先根据本地数据库进行初始化
        final User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
        final List<SingleContact> singleContactList = user.getSingleContactList();
        for(SingleContact singleContact : singleContactList){
            if(singleContact.getIsGroup() == 0)
                postList.addAll(singleContact.getPost_list());
        }
        postList.addAll(user.getPostList());

        /*OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http" + Singleton.getInstance().getUrl() + "/api/moment/all")
                .addHeader("Cookie",Singleton.getInstance().getHeader())
                .get()
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String tempresponse = response.body().string();
                //Log.d("TEST", tempresponse);
                Gson gson = new Gson();
                MomentResponse momentResponse = gson.fromJson(tempresponse, MomentResponse.class);
                if (momentResponse.getMessage() != null) {

                    List<MomentResponseMessage> momentResponseMessages = momentResponse.getMessage();
                    for (MomentResponseMessage momentResponseMessage : momentResponseMessages) {
                        int exist = 0;
                        for (Post post : postList) {
                            if (post.getMid().equals(momentResponseMessage.getMid())) {
                                ++exist;
                                break;
                            }
                        }
                        if (exist == 0) {
                            SingleContact singleContact = new SingleContact();
                            int ffound = 0;
                            for (SingleContact singleContact1 : singleContactList) {
                                if (singleContact1.getM_id().equals(momentResponseMessage.getPublisherID())) {
                                    singleContact = singleContact1;
                                    ffound = 1;
                                    break;
                                }
                            }
                            if (ffound == 0) {
                                Post post = new Post();
                                post.setMid(momentResponseMessage.getMid());
                                post.setPost_text(momentResponseMessage.getContent());
                                post.setPoster_name(user.getName());
                                post.setUser_id(user.getId());
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                                Date date = new Date(Long.parseLong(momentResponseMessage.getPublishedTime()) * 1000);//时间戳转换为long
                                String time = simpleDateFormat.format(date);
                                post.setPost_time(time);
                                post.setLikers(momentResponseMessage.getLikes());
                                post.save();
                                postList.add(post);
                            } else {
                                Post post = new Post();
                                post.setMid(momentResponseMessage.getMid());
                                post.setPost_text(momentResponseMessage.getContent());
                                post.setPoster_name(singleContact.getName());
                                post.setSinglecontact_id(singleContact.getId());
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                                Date date = new Date(Long.parseLong(momentResponseMessage.getPublishedTime()) * 1000);//时间戳转换为long
                                String time = simpleDateFormat.format(date);
                                post.setPost_time(time);
                                post.setLikers(momentResponseMessage.getLikes());
                                post.save();
                                postList.add(post);
                            }
                        }
                    }
                }
            }

        });*/




    }
}