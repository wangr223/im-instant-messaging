package com.example.ruichatver1_0;

import java.security.acl.Owner;
import java.util.List;

public class GroupAllResMessage {
    private String Number;
    private String Name;
    private String Ownerid;
    private List<String> Members;
    private List<String> Membersname;
    private String Announcement;

    public List<String> getMembersname() {
        return Membersname;
    }

    public void setMembersname(List<String> membersname) {
        Membersname = membersname;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getOwnerid() {
        return Ownerid;
    }

    public void setOwnerid(String ownerid) {
        Ownerid = ownerid;
    }

    public List<String> getMembers() {
        return Members;
    }

    public void setMembers(List<String> members) {
        Members = members;
    }

    public String getAnnouncement() {
        return Announcement;
    }

    public void setAnnouncement(String announcement) {
        Announcement = announcement;
    }
}
