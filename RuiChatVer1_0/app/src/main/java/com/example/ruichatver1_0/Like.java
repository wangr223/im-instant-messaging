package com.example.ruichatver1_0;

public class Like {
    private String Lid;
    private String LikerID;
    private String Mid;

    public String getLid() {
        return Lid;
    }

    public void setLid(String lid) {
        Lid = lid;
    }

    public String getLikerID() {
        return LikerID;
    }

    public void setLikerID(String likerID) {
        LikerID = likerID;
    }

    public String getMid() {
        return Mid;
    }

    public void setMid(String mid) {
        Mid = mid;
    }

}
