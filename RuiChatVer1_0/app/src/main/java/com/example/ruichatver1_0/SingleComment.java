package com.example.ruichatver1_0;

import org.litepal.crud.LitePalSupport;

import java.security.PrivateKey;

public class SingleComment extends LitePalSupport {
    private int id;
    private String PublisherName;
    private String Content;


    private int post_id;
    private Post post;

    public String getPublisherName() {
        return PublisherName;
    }

    public void setPublisherName(String publisherName) {
        PublisherName = publisherName;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }


}
