package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.io.IOException;
import java.net.URI;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddNFwithEmail extends AppCompatActivity {
    private Button cancel;
    private Button confirm;
    private EditText emailTV;
    private String res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Log.i("TEST","OnAddCreate();");
        super.onCreate(savedInstanceState);
        //Log.i("TEST","OnAddCreate2();");
        setContentView(R.layout.activity_add_n_fwith_email);
        //Log.i("TEST","OnAddCreate3();");
        ActionBar acbar = getSupportActionBar();
        if(acbar != null) acbar.hide();
        //Log.i("TEST","OnAddCreate4();");
        cancel = findViewById(R.id.friend_title_back);
        confirm = findViewById(R.id.ANF_confirm);
        emailTV = findViewById(R.id.ANF_email);
        res = "";

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddNFwithEmail.this, MyFriend.class);
                startActivity(intent);
                AddNFwithEmail.this.finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailTV.getText().toString();
                if (!"".equals(email)) {
                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("email", email);
                    OkHttpClient okHttpClient = new OkHttpClient();
                    RequestBody body = RequestBody.Companion.create(jsonObject.toString(), JSON);
                    Request request = new Request.Builder()
                            .url("http" + Singleton.getInstance().getUrl() + "/api/user/addfriend")
                            .addHeader("Cookie",Singleton.getInstance().getHeader())
                            .post(body)
                            .build();
                    okHttpClient.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(@NotNull Call call, @NotNull IOException e) {
                            Log.e("TEST","NF OKHttpSend failed");
                        }

                        @Override
                        public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                            String tempresponse = response.body().string();
                            //Log.d("TEST" , tempresponse);
                            Gson gson = new Gson();
                            LogInResponse LoginRes = gson.fromJson(tempresponse, LogInResponse.class);
                            Log.d("TEST","ANF_Response :  " + LoginRes.getMessage());
                            if( "success".equals(LoginRes.getType())){
                                res = "添加成功，等待验证！";
                            }
                            else{
                                if("friend has exist".equals(LoginRes.getMessage()))
                                    res = "好友已经存在！";
                                else if("friend not exist".equals(LoginRes.getMessage()))
                                    res = "此邮箱不存在！";
                            }
                            Intent intent = new Intent(AddNFwithEmail.this, MyFriend.class);
                            intent.putExtra("res",res);
                            intent.putExtra("yes",1);
                            startActivity(intent);
                            AddNFwithEmail.this.finish();
                        }
                    });
                }
            }
        });


    }
}