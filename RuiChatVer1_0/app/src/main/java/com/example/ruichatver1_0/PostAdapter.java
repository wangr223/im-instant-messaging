package com.example.ruichatver1_0;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder>{
    private List<Post> postList;
    private Context context;

    static class ViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{

        ImageView p_head;
        TextView p_name;
        TextView p_content;
        ImageView p_img1;
        ImageView p_img2;
        ImageView p_img3;
        TextView p_time;
        TextView likers;
        CommentsView LLComments;
        Button like;
        Button comment;

        public ViewHolder(View view) {
            super(view);
            p_head = view.findViewById(R.id.post_head);
            p_name = view.findViewById(R.id.post_name);
            p_content = view.findViewById(R.id.post_content);
            p_img1 = view.findViewById(R.id.post_img1);
            p_img2 = view.findViewById(R.id.post_img2);
            p_img3 = view.findViewById(R.id.post_img3);
            p_time = view.findViewById(R.id.post_time);
            like = view.findViewById(R.id.post_like);
            comment = view.findViewById(R.id.post_comment);
            likers = view.findViewById(R.id.post_Likers);
            LLComments = view.findViewById(R.id.post_comments);
            /*comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("TEST","Success");
                }
            });*/
            comment.setOnClickListener(this);
            like.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
                if(v.getId() == R.id.post_like) likeOnItemClickListener.onItemClick(v,getAdapterPosition());
                else commentOnItemClickListener.onItemClick(v,getAdapterPosition());
        }

    }

    //add another button for the Comment

    public static OnItemClickListener likeOnItemClickListener;
    public static OnItemClickListener commentOnItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(View v, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener, OnItemClickListener listener1){
        this.likeOnItemClickListener = listener;
        this.commentOnItemClickListener = listener1;
    }

    public PostAdapter(List<Post> postList, Context context){
        this.postList = postList;
        this.context = context;
    }

    @NonNull
    @Override
    public PostAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_post,parent,false);
        return new PostAdapter.ViewHolder(view);
    }

    public static String listToString(List<String> list){

        if(list==null){
            return null;
        }

        StringBuilder result = new StringBuilder();
        boolean first = true;

        //第一个前面不拼接","
        for(String string :list) {
            if(first || string.equals("点赞 ：")) {
                first=false;
            }else{
                result.append(", ");
            }
            result.append(string);
        }
        return result.toString();
    }

    @Override
    public void onBindViewHolder(@NonNull PostAdapter.ViewHolder holder, int position) {
        Post post = postList.get(position);
        if (post.getLiked() == 1){
            holder.like.setBackgroundResource(R.drawable.heart_clicked);
        }
        if (post.getLikers().size() != 0){
            holder.likers.setVisibility(View.VISIBLE);
            String content = "";
            content = listToString(post.getLikers());
            if (!content.equals("")) {
                StringBuilder result = new StringBuilder();
                result.append("点赞 ：");
                result.append(content);
                String full = result.toString();
                holder.likers.setText(full);
            }
            if (holder.likers.getText().toString().equals("")){
                holder.likers.setVisibility(View.GONE);
            }
        }
        if (post.getCommentList().size() != 0){
            System.out.println(post.getCommentList().size());
            holder.LLComments.setVisibility(View.VISIBLE);
            holder.LLComments.setList(post.getCommentList());
            holder.LLComments.notifyDataSetChanged();
        }
        holder.p_head.setImageBitmap(stringToBitmap(post.getPoster_head()));
        System.out.println("Content ---->  "  + post.getPost_text()  +  ",  HeadString -----> "  + post.getPoster_head());
        holder.p_name.setText(post.getPoster_name());
        holder.p_content.setText(post.getPost_text());
        int n_img = post.getPost_imageList().size();
        System.out.println("Content ----> " + post.getPost_text()  + ", Num of photo -----> " + n_img);
        switch (n_img){
            case 1:
                holder.p_img1.setVisibility(View.VISIBLE);
                holder.p_img1.setImageBitmap(stringToBitmap(post.getPost_imageList().get(0)));
                break;
            case 2:
                holder.p_img1.setVisibility(View.VISIBLE);
                holder.p_img1.setImageBitmap(stringToBitmap(post.getPost_imageList().get(0)));
                holder.p_img2.setVisibility(View.VISIBLE);
                holder.p_img2.setImageBitmap(stringToBitmap(post.getPost_imageList().get(1)));
                break;
            case 3:
                holder.p_img1.setVisibility(View.VISIBLE);
                holder.p_img1.setImageBitmap(stringToBitmap(post.getPost_imageList().get(0)));
                holder.p_img2.setVisibility(View.VISIBLE);
                holder.p_img2.setImageBitmap(stringToBitmap(post.getPost_imageList().get(1)));
                holder.p_img3.setVisibility(View.VISIBLE);
                holder.p_img3.setImageBitmap(stringToBitmap(post.getPost_imageList().get(2)));
                break;
        }
        holder.p_time.setText(post.getPost_time());


    }

    public Bitmap stringToBitmap(String string) {
        // 将字符串转换成Bitmap类型
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray;
            bitmapArray = Base64.decode(string, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
                    bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }
}
