package com.example.ruichatver1_0;

public class UserSelfResponseData {
    private String Email;
    private String Password;
    private UserInfo Info;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public UserInfo getUserInfo() {
        return Info;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.Info = userInfo;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
