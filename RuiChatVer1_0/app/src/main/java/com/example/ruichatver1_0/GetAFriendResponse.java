package com.example.ruichatver1_0;

public class GetAFriendResponse {
    private int Code;
    private String State;
    private UserInfo Message;

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public UserInfo getMessage() {
        return Message;
    }

    public void setMessage(UserInfo message) {
        Message = message;
    }
}
