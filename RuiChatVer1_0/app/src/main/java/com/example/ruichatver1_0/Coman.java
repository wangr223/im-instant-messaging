package com.example.ruichatver1_0;

public class Coman {
    private String Cid;
    private String Content;
    private String PublisherID;
    private String PublishedTime;

    public String getCid() {
        return Cid;
    }

    public void setCid(String cid) {
        Cid = cid;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getPublisherID() {
        return PublisherID;
    }

    public void setPublisherID(String publisherID) {
        PublisherID = publisherID;
    }

    public String getPublishedTime() {
        return PublishedTime;
    }

    public void setPublishedTime(String publishedTime) {
        PublishedTime = publishedTime;
    }

}
