package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CreateGC extends AppCompatActivity {
    private Button cancel;
    private Button confirm;
    private EditText nameTV;
    private String res_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_g_c);

        ActionBar acbar = getSupportActionBar();
        if(acbar != null ){
            acbar.hide();
        }

        cancel = findViewById(R.id.friend_title_back);
        confirm = findViewById(R.id.CGC_confirm);
        nameTV = findViewById(R.id.CGC_name);
        res_ = "";

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateGC.this, MyFriend.class);
                startActivity(intent);
                CreateGC.this.finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String g_name = nameTV.getText().toString();
                if (!"".equals(g_name)) {
                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("name", g_name);
                    OkHttpClient okHttpClient = new OkHttpClient();
                    RequestBody body = RequestBody.Companion.create(jsonObject.toString(), JSON);
                    Request request = new Request.Builder()
                            .url("http" + Singleton.getInstance().getUrl() + "/api/group/create")
                            .addHeader("Cookie",Singleton.getInstance().getHeader())
                            //.post(body)
                            .post(body)
                            .build();
                    okHttpClient.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(@NotNull Call call, @NotNull IOException e) {
                            Log.e("TEST","NF OKHttpSend failed");
                        }

                        @Override
                        public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                            String tempresponse = response.body().string();
                            //Log.d("TEST" , tempresponse);
                            Gson gson = new Gson();
                            LogInResponse LoginRes = gson.fromJson(tempresponse, LogInResponse.class);
                            //Log.d("TEST","CGC_Response :  " + LoginRes.getMessage());
                            if( "success".equals(LoginRes.getType())){
                                res_ = "创建成功";
                            }
                            Intent intent = new Intent(CreateGC.this, GroupChatList.class);
                            intent.putExtra("res_",res_);
                            intent.putExtra("yes_",1);
                            startActivity(intent);
                            CreateGC.this.finish();
                        }
                    });
                }
            }
        });
    }
}