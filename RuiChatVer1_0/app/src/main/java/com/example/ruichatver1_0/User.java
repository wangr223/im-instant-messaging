package com.example.ruichatver1_0;

import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;

import java.sql.Struct;
import java.util.List;

public class User extends LitePalSupport {
    private int id;
    private String Uid;
    private String name;
    private String mood;
    private String email;
    private String sex;
    private String password;
    private String head;
    List<Post> postList;
    List<SingleContact> singleContactList;

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUid() {
        return Uid;
    }

    public void setUid(String uid) {
        Uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public List<SingleContact> getSingleContactList() {
        return LitePal.where("user_id = ?", String.valueOf(id)).find(SingleContact.class);
        //return singleContactList;
    }

    public void setSingleContactList(List<SingleContact> singleContactList) {
        this.singleContactList = singleContactList;
    }

    public List<Post> getPostList() {
        return LitePal.where("user_id = ?", String.valueOf(id)).find(Post.class);
        //return postList;
    }

    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
