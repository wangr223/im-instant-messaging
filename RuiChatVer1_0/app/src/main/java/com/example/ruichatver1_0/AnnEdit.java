package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.GnssNavigationMessage;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.litepal.LitePal;

import java.io.IOException;

import javax.crypto.AEADBadTagException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AnnEdit extends AppCompatActivity {
    private Button cancel;
    private Button confirm;
    private EditText ann_text;
    private String G_Num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ann_edit);
        ActionBar acbar = getSupportActionBar();
        if (acbar != null)
            acbar.hide();

        cancel = findViewById(R.id.title_cancel);
        confirm = findViewById(R.id.title_confirm);
        ann_text = findViewById(R.id.ann_content);

        Intent intent_ = this.getIntent();
        G_Num = intent_.getStringExtra("G_Num");

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AnnEdit.this, GroupSpace.class);
                intent.putExtra("G_Number", G_Num);
                startActivity(intent);
                AnnEdit.this.finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ann = ann_text.getText().toString();
                Log.d("TEST",ann);
                if ("".equals(ann)) {
                    Toast.makeText(AnnEdit.this, "群公告不可为空", Toast.LENGTH_SHORT).show();
                }
                else{
                    User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                    SingleContact singleContact = LitePal.where("user_id = ? and G_Number = ?", String.valueOf(user.getId()), G_Num)
                            .findFirst(SingleContact.class);
                    singleContact.setAnnouncement(ann);
                    singleContact.update(singleContact.getId());


                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("number", G_Num);
                    jsonObject.addProperty("announcement", ann);
                    OkHttpClient okHttpClient = new OkHttpClient();
                    final RequestBody body = RequestBody.Companion.create(jsonObject.toString(), JSON);
                    Request request = new Request.Builder()
                            .url("http" + Singleton.getInstance().getUrl() + "/api/group/announcement" )
                            .addHeader("Cookie",Singleton.getInstance().getHeader())
                            .post(body)
                            .build();
                    okHttpClient.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        }

                        @Override
                        public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                            String tempresponse = response.body().string();
                            Log.d("TEST","AnnEdit Response : " + tempresponse);
                            final Gson gson = new Gson();
                            LogInResponse LoginRes = gson.fromJson(tempresponse, LogInResponse.class);
                            if (LoginRes.getType().equals("success")){
                                Intent intent = new Intent(AnnEdit.this, GroupSpace.class);
                                intent.putExtra("G_Number", G_Num);
                                startActivity(intent);
                                AnnEdit.this.finish();
                            }
                        }
                    });
                }

            }
        });
    }
}