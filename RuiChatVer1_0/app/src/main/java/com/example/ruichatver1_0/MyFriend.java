package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.promeg.pinyinhelper.Pinyin;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.litepal.LitePal;
import org.litepal.tablemanager.Connector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MyFriend extends AppCompatActivity {
    private List<AFriend> fl = new ArrayList<>();
    private Button to_mymsg;
    private Button to_myspace;
    private List<SingleContact> contactList = new ArrayList<>();
    private RelativeLayout NF;
    private RelativeLayout GC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_friend);
        ActionBar acbar = getSupportActionBar();
        if(acbar != null) acbar.hide();
        //Log.d("TEST",Singleton.getInstance().getServer_Uid_me());

        Intent intent = this.getIntent();
        int yes = intent.getIntExtra("yes",0);
        String add_result = intent.getStringExtra("res");
        if( yes == 1)
            Toast.makeText(this, add_result, Toast.LENGTH_SHORT).show();

        Connector.getDatabase();


        Local_init();
        Web_init();
        //initFriends();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.friends_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        //Log.d("TEST",fl.get(0).getname());
        AFriendAdapter adapter = new AFriendAdapter(fl);
        recyclerView.setAdapter(adapter);


        NF = findViewById(R.id.my_space_function_NF);
        GC = findViewById(R.id.my_space_function_GC);

        NF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MyFriend.this, "NF", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MyFriend.this, AddNFwithEmail.class);
                startActivity(intent);
                MyFriend.this.finish();
            }
        });

        GC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MyFriend.this, "GC", Toast.LENGTH_SHORT).show();
                //preWeb_init();
                Log.d("TEST","Clicked");
                Intent intent = new Intent(MyFriend.this, GroupChatList.class);
                startActivity(intent);
                MyFriend.this.finish();
            }
        });

        /*RecyclerView recyclerView = (RecyclerView) findViewById(R.id.friends_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);*/

        /*AFriendAdapter adapter = new AFriendAdapter(fl);
        recyclerView.setAdapter(adapter);*/

        adapter.setOnItemClickListener(mClickListener);
        to_mymsg = (Button) findViewById(R.id.my_msg);
        to_myspace = (Button) findViewById(R.id.my_space);

        to_mymsg.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MyFriend.this,friendList.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                MyFriend.this.finish();
            }
        });

        to_myspace.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MyFriend.this,MySpace.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                MyFriend.this.finish();
            }
        });
    }


    private AFriendAdapter.OnItemClickListener mClickListener = new AFriendAdapter.OnItemClickListener(){
        @Override
        public void onItemClick(View v, int position) {
            OkHttpClient okHttpClient = new OkHttpClient();
            final String singleID = fl.get(position).getF_ID();
            Request request = new Request.Builder()
                    .addHeader("Cookie", Singleton.getInstance().getServer_Uid_me())
                    .url("http" + Singleton.getInstance().getUrl() + "/api/user/id/" + singleID)
                    .get()
                    .build();
            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    String tempresponse = response.body().string();
                    //Log.d("TEST",tempresponse);
                    Gson gson = new Gson();
                    GetAFriendResponse getAFriendResponse = gson.fromJson(tempresponse, GetAFriendResponse.class);
                    User tempUser = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                    int user_id = tempUser.getId();
                    List<SingleContact> tempUserFriends = tempUser.getSingleContactList();
                    int exist = 0;
                    for(SingleContact singleContact : tempUserFriends) {
                        if (singleID.equals(singleContact.getM_id())) {
                            ++exist;
                            singleContact.setHead(getAFriendResponse.getMessage().getAvatar());
                            singleContact.setName(getAFriendResponse.getMessage().getName());
                            singleContact.setEmail(getAFriendResponse.getMessage().getEmail());
                            singleContact.setSex(getAFriendResponse.getMessage().getGender() == 1 ? "男" : "女");
                            singleContact.setMood(getAFriendResponse.getMessage().getBio());
                            singleContact.update(singleContact.getId());
                            break;
                        }
                    }
                    if(exist == 0){
                        SingleContact singleContact = new SingleContact();
                        singleContact.setM_id(singleID);
                        singleContact.setHead(getAFriendResponse.getMessage().getAvatar());
                        singleContact.setSex(getAFriendResponse.getMessage().getGender() == 1 ? "男" : "女");
                        singleContact.setMood(getAFriendResponse.getMessage().getBio());
                        singleContact.setEmail(getAFriendResponse.getMessage().getEmail());
                        singleContact.setName(getAFriendResponse.getMessage().getName());
                        singleContact.setUser_id(user_id);
                        singleContact.save();
                    }
                    //Log.d("TEST",contactList.get(0).getName());
                }
            });
            Intent intent = new Intent(MyFriend.this,FriendSpace.class);
            intent.putExtra("FID", fl.get(position).getF_ID());
            startActivity(intent);
        }
    };

    private void initFriends() {
        for(SingleContact someone : contactList){
            AFriend aFriend = new AFriend(someone.getHead(), someone.getName(), Pinyin.toPinyin(someone.getName().charAt(0)).toUpperCase().charAt(0));
            aFriend.setF_ID(someone.getM_id());
            fl.add(aFriend);
        }

        int len = fl.size();
        for(int i = 1 ; i < len ; i++){
            for(int j = i ; j > 0 &&
                    Pinyin.toPinyin(fl.get(j).getname().charAt(0)).toUpperCase().compareTo(Pinyin.toPinyin(fl.get(j-1).getname().charAt(0)).toUpperCase()) <= 0
                    ;
                j--){
                AFriend aFriend = new AFriend(fl.get(j).getFrd_head(),
                        fl.get(j).getname(),
                        fl.get(j).getFrd_topChar());
                aFriend.setF_ID(fl.get(j).getF_ID());
                fl.set(j,fl.get(j-1));
                fl.set(j-1,aFriend);
            }
        }
        for(int i = len - 1 ; i > 0 ; i--){
            for(int j = i ; j > 0; j--) {
                if(Pinyin.toPinyin(fl.get(j-1).getname().charAt(0)).charAt(0) == Pinyin.toPinyin(fl.get(j).getname().charAt(0)).charAt(0) ){
                    if(isbiggerBoolean(Pinyin.isChinese(fl.get(j-1).getname().charAt(0)), Pinyin.isChinese(fl.get(j).getname().charAt(0))) == 1){
                        AFriend aFriend = new AFriend(fl.get(j).getFrd_head(),
                            fl.get(j).getname(),
                                fl.get(j).getFrd_topChar());
                        aFriend.setF_ID(fl.get(j).getF_ID());
                        fl.set(j, fl.get(j - 1));
                        fl.set(j - 1, aFriend);
                    }
                    else break;
                }
            }
        }

    }

    private int isbiggerBoolean( Boolean a, Boolean b){
        if ( a && !b )
            return 1;
        return  0;
    }

    private void Local_init() {
        User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
        contactList = LitePal.where("user_id = ? and isGroup = ?", String.valueOf(user.getId()),"0").find(SingleContact.class);
    }
    private void Web_init(){
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http" + Singleton.getInstance().getUrl() + "/api/user/friends")
                .addHeader("Cookie",Singleton.getInstance().getHeader())
                .get()
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Local_init();
                initFriends();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String tempresponse =
                        response.body().string();
                Gson gson = new Gson();
                GetFriendsResponse getFriendsResponse = gson.fromJson(tempresponse, GetFriendsResponse.class);
                List<String> friendIDs = getFriendsResponse.getData().getFriends();
                for(final String singleID : friendIDs){
                    OkHttpClient okHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .addHeader("Cookie", Singleton.getInstance().getServer_Uid_me())
                            .url("http" + Singleton.getInstance().getUrl() + "/api/user/id/" + singleID)
                            .get()
                            .build();
                    okHttpClient.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        }

                        @Override
                        public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                            String tempresponse = response.body().string();
                            Gson gson = new Gson();
                            GetAFriendResponse getAFriendResponse = gson.fromJson(tempresponse, GetAFriendResponse.class);
                            User tempUser = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                            int user_id = tempUser.getId();
                            List<SingleContact> tempUserFriends = tempUser.getSingleContactList();
                            int exist = 0;
                            for(SingleContact singleContact : tempUserFriends) {
                                if (singleID.equals(singleContact.getM_id())) {
                                    ++exist;
                                    /*singleContact.setName(getAFriendResponse.getMessage().getName());
                                    singleContact.setEmail(getAFriendResponse.getMessage().getEmail());
                                    singleContact.setSex(getAFriendResponse.getMessage().getGender() == 1 ? "男" : "女");
                                    singleContact.setMood(getAFriendResponse.getMessage().getBio());
                                    singleContact.update(singleContact.getId());*/
                                    break;
                                }
                            }
                            if(exist == 0){
                                SingleContact singleContact = new SingleContact();
                                singleContact.setM_id(singleID);
                                singleContact.setHead(getAFriendResponse.getMessage().getAvatar());
                                singleContact.setSex(getAFriendResponse.getMessage().getGender() == 1 ? "男" : "女");
                                singleContact.setMood(getAFriendResponse.getMessage().getBio());
                                singleContact.setEmail(getAFriendResponse.getMessage().getEmail());
                                singleContact.setName(getAFriendResponse.getMessage().getName());
                                singleContact.setUser_id(user_id);
                                singleContact.save();
                            }
                        }
                    });
                }
                {
                    User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                    contactList = LitePal.where("user_id = ? and isGroup = ?", String.valueOf(user.getId()),"0").find(SingleContact.class);
                    initFriends();
                }
            }
        });
    }
}