package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.List;

public class FriendMoment extends AppCompatActivity {

    private String FID;
    private String name;
    private TextView friend_name;
    private Button back;
    private SingleContact thisContact;
    private List<Post> postList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_moment);
        ActionBar acbar = getSupportActionBar();
        if(acbar != null) acbar.hide();

        FID = this.getIntent().getStringExtra("FID");
        int user_id = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me())
                .findFirst(User.class).getId();
        thisContact = LitePal.where("user_id = ? and m_id = ?", String.valueOf(user_id) , FID)
                .findFirst(SingleContact.class);
        name = thisContact.getName();
        friend_name = findViewById(R.id.friend_title_text);
        back = findViewById(R.id.friend_title_back);
        friend_name.setText(name);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendMoment.this.finish();
            }
        });

        initPosts();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.friend_post);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        PostAdapter adapter = new PostAdapter(postList,this);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(LikeItemClickListener, CommentItemClickListener);

    }

    private PostAdapter.OnItemClickListener LikeItemClickListener = new PostAdapter.OnItemClickListener(){
        @Override
        public void onItemClick(View v, int position) {
            Post post = postList.get(position);
            if(post.getLiked() == 0) {
                v.setBackgroundResource(R.drawable.heart_clicked);
                postList.get(position).like_it();
            }
            else{
                v.setBackgroundResource(R.drawable.heart);
                postList.get(position).like_cancel();
            }
        }
    };

    private PostAdapter.OnItemClickListener CommentItemClickListener = new PostAdapter.OnItemClickListener(){
        @Override
        public void onItemClick(View v, int position) {
            Log.d("TEST","Comment");/////////////////////////Pop typewriting and finish for real comment;
        }
    };

    public void initPosts(){
        postList = thisContact.getPost_list();
    }
}