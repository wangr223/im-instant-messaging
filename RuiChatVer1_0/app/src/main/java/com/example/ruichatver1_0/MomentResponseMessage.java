package com.example.ruichatver1_0;


import java.util.ArrayList;
import java.util.List;

public class MomentResponseMessage {
    private String Mid;
    private List<String> Image;
    private String Content;
    private String PublisherID;
    private String PublishedTime;
    private String PublisherName;
    private String Avatar;
    //private Coman [] comans;
    private List<String> LikeNames = new ArrayList<>();
    private List<SingleComment> Comments = new ArrayList<>();

    public String getPublisherName() {
        return PublisherName;
    }

    public void setPublisherName(String publisherName) {
        PublisherName = publisherName;
    }

    public String getAvata() {
        return Avatar;
    }

    public void setAvata(String avata) {
        Avatar = avata;
    }

    public List<SingleComment> getComments() {
        return Comments;
    }

    public void setComments(List<SingleComment> comments) {
        Comments = comments;
    }

    public List<String> getLikes() {
        return LikeNames;
    }

    public void setLikes(List<String> likes) {
        LikeNames = likes;
    }

    public String getMid() {
        return Mid;
    }

    public void setMid(String mid) {
        Mid = mid;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getPublisherID() {
        return PublisherID;
    }

    public void setPublisherID(String publisherID) {
        PublisherID = publisherID;
    }

    public String getPublishedTime() {
        return PublishedTime;
    }

    public void setPublishedTime(String publishedTime) {
        PublishedTime = publishedTime;
    }

    public List<String> getImage() {
        return Image;
    }

    public void setImage(List<String> image) {
        Image = image;
    }
}


