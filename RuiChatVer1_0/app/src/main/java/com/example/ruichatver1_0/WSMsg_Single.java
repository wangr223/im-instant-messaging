package com.example.ruichatver1_0;

public class WSMsg_Single {
    private String Sendid;
    private String Content;
    private String PublishedTime;


    /////
    private String Recid;
    private String Type;
    private String Name;
    private String Avatar;
    private String Image;

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String avatar) {
        Avatar = avatar;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getRecid() {
        return Recid;
    }

    public void setRecid(String recid) {
        Recid = recid;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getSendid() {
        return Sendid;
    }

    public void setSendid(String sendid) {
        Sendid = sendid;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getPublishedTime() {
        return PublishedTime;
    }

    public void setPublishedTime(String publishedTime) {
        PublishedTime = publishedTime;
    }
}
