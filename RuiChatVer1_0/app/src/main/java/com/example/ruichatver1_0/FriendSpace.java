package com.example.ruichatver1_0;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.utils.widget.ImageFilterView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.litepal.LitePal;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FriendSpace extends AppCompatActivity {
    private Button back_to_MF;
    private Button send_msg;
    private Button to_friend_moment;
    private ImageView friend_head_img;
    private TextView friend_name;
    private TextView friend_id;
    private TextView friend_sex;
    private TextView friend_email;
    private TextView friend_mood;
    private String FID;

    private String name;
    private String mood;
    private String email;
    private String sex;
    private String id;
    private SingleContact singleContact;


    public Bitmap stringToBitmap(String string) {
        // 将字符串转换成Bitmap类型
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray;
            bitmapArray = Base64.decode(string, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
                    bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_space);
        ActionBar acbar = getSupportActionBar();
        if(acbar != null ){
            acbar.hide();
        }

        FID = this.getIntent().getStringExtra("FID");

        send_msg = findViewById(R.id.friend_send_msg);
        to_friend_moment = findViewById(R.id.to_friend_moment);
        friend_head_img = findViewById(R.id.friend_space_head);
        friend_name = findViewById(R.id.friend_space_name);
        friend_id = findViewById(R.id.friend_space_id);
        friend_sex = findViewById(R.id.friend_space_sex);
        friend_email = findViewById(R.id.friend_space_email);
        friend_mood = findViewById(R.id.friend_space_mood);
        back_to_MF = findViewById(R.id.friend_title_back);


        singleContact = LitePal.where("m_id = ?", FID).findFirst(SingleContact.class);

        name = singleContact.getName();
        sex = singleContact.getSex();
        email = singleContact.getEmail();
        mood = singleContact.getMood();
        id = singleContact.getM_id();

        friend_head_img.setImageBitmap(stringToBitmap(singleContact.getHead()));
        friend_name.setText(name);
        friend_sex.setText(sex);
        friend_email.setText(email);
        friend_mood.setText(mood);
        friend_id.setText(id);

        back_to_MF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FriendSpace.this, MyFriend.class);
                startActivity(intent);
                FriendSpace.this.finish();
            }
        });

        send_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FriendSpace.this, Chatting.class);
                intent.putExtra("FID", FID);
                intent.putExtra("IsGroup",0);
                startActivity(intent);
            }
        });

        to_friend_moment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ////////OKHttp there and update the database
                OkHttpClient okHttpClient = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http" + Singleton.getInstance().getUrl() + "/api/moment/all/" + singleContact.getM_id())
                        .addHeader("Cookie",Singleton.getInstance().getHeader())
                        .get()
                        .build();
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        String tempresponse = response.body().string();
                        //Log.d("TEST",tempresponse);
                        Gson gson = new Gson();
                        MomentResponse momentResponse = gson.fromJson(tempresponse, MomentResponse.class);
                        if( momentResponse.getMessage() != null){
                            List<MomentResponseMessage> momentResponseMessages = momentResponse.getMessage();
                            //System.out.println(momentResponseMessages.size());
                            List<Post> postList = singleContact.getPost_list();
                            for(MomentResponseMessage momentResponseMessage : momentResponseMessages){
                                int exist = 0;
                                for(Post post : postList){
                                    if(post.getMid().equals(momentResponseMessage.getMid())){
                                        ++exist;
                                        Post post_ = new Post();
                                        post_.setLikers(momentResponseMessage.getLikes());
                                        //post_.setCommentList(momentResponseMessage.getComments());
                                        LitePal.deleteAll(SingleComment.class, "post_id = ?", String.valueOf(post.getId()));
                                        List<SingleComment> singleCommentList = momentResponseMessage.getComments();
                                        for(SingleComment singleComment : singleCommentList){
                                            singleComment.setPost_id(post.getId());
                                            singleComment.save();
                                        }
                                        post_.setPoster_name(momentResponseMessage.getPublisherName());
                                        post_.setPoster_head(momentResponseMessage.getAvata());
                                        post_.update(post.getId());
                                        break;
                                    }
                                }
                                if(exist == 0){//如果存在也需要进行更新
                                    Post post = new Post();
                                    post.setMid(momentResponseMessage.getMid());
                                    post.setPost_text(momentResponseMessage.getContent());
                                    post.setPoster_name(momentResponseMessage.getPublisherName());
                                    post.setSinglecontact_id(singleContact.getId());
                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                                    Date date = new Date(Long.parseLong(momentResponseMessage.getPublishedTime()) * 1000);//时间戳转换为long
                                    //System.out.println(System.currentTimeMillis());
                                    String time = simpleDateFormat.format(date);
                                    post.setPost_time(time);
                                    List<SingleComment> singleCommentList = momentResponseMessage.getComments();
                                    for(SingleComment singleComment : singleCommentList){
                                        singleComment.setPost_id(post.getId());
                                        singleComment.save();
                                    }
                                    post.setCommentList(momentResponseMessage.getComments());
                                    post.setLikers(momentResponseMessage.getLikes());
                                    post.setPost_imageList(momentResponseMessage.getImage());
                                    post.setPoster_head(momentResponseMessage.getAvata());
                                    post.save();
                                    postList.add(post);
                                }
                            }
                        }
                    }

                });
                Intent intent = new Intent(FriendSpace.this, FriendMoment.class);
                intent.putExtra("FID", FID);
                startActivity(intent);
            }
        });

    }
}