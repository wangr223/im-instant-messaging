package com.example.ruichatver1_0;
///需要数据库存放已经保存的账号与密码
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.litepal.crud.LitePalSupport;
import org.litepal.tablemanager.Connector;
import org.litepal.LitePal;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Log_in extends AppCompatActivity {
    private EditText username;
    private EditText password;
    private Button signin;
    private Button register;
    private int wrong;
    //private List<SingleContact> contactList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        wrong = 0;
        Connector.getDatabase();
        initContacts();

        ActionBar acbar = getSupportActionBar();
        if(acbar != null ){
            acbar.hide();
        }

        Intent intent_ = this.getIntent();
        if( intent_.getIntExtra("wrong", 0) == 1){
            Toast.makeText(this, intent_.getStringExtra("report"), Toast.LENGTH_SHORT).show();
        }

        /*String tempresponse =
                //response.body().string()
                 "{ \"Code\":200, \"State\":\"success\", \"userSelfResponseData\":{\"Email\":\"123456\",\"userInfo\":{\"name\":\"cc\",\"bio\":\"mood\",\"Gender\":1}}}";
        Log.d("TEST",tempresponse);
        Gson gson = new Gson();
        UserSelfResponse userSelfResponse = gson.fromJson(tempresponse, UserSelfResponse.class);
        System.out.println(userSelfResponse.getUserSelfResponseData().getUserInfo().getGender());*/
        //Log.d("TEST",userSelfResponse.getUserSelfResponseData().getUserInfo().getName());


        Singleton.getInstance().setUrl("://172.18.43.190:9999");
        int rgis = 0;
        final Intent intent = getIntent();
        rgis = intent.getIntExtra("result", 0);
        if (rgis == 1)
            Toast.makeText(Log_in.this, "注册成功！",Toast.LENGTH_SHORT).show();
        username = (EditText) findViewById(R.id.account);
        password = (EditText) findViewById(R.id.password);
        signin =  (Button) findViewById(R.id.denglu);
        register = (Button) findViewById(R.id.zhuce);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                final String tempemail = username.getText().toString();
                final String temppass = password.getText().toString();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("name",tempemail);
                jsonObject.addProperty("password",temppass);
                OkHttpClient okHttpClient = new OkHttpClient();
                final RequestBody body = RequestBody.Companion.create(jsonObject.toString(), JSON);
                Request request = new Request.Builder()
                        .url("http" + Singleton.getInstance().getUrl() + "/api/user/login")
                        .post(body)
                        .build();
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        String tempresponse = response.body().string();
                        Gson gson = new Gson();
                        LogInResponse LoginRes = gson.fromJson(tempresponse, LogInResponse.class);
                        Headers responseHeaders = response.headers();
                        String headerName = responseHeaders.name(1);
                        String headerValue = responseHeaders.get(headerName);
                        Singleton.getInstance().setHeader(headerValue);
                        Singleton.getInstance().setServer_Uid_me(LoginRes.getMessage());
                        if(LoginRes.getType().equals("success")){
                            URI uri = URI.create("ws" + Singleton.getInstance().getUrl() + "/api/user/connect");
                            Singleton.getInstance().setClient(new JWebSocketClient(uri){
                                @Override
                                public void onMessage(String message) {
                                    if( !"heartbeat".equals(message)) {
                                        User user = LitePal.where("Uid =?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                                        SingleContact singleContact = new SingleContact();
                                        int S_id = 0;
                                        Gson gson1 = new Gson();
                                        WSMsg_Single wsMsg_single = gson1.fromJson(message, WSMsg_Single.class);
                                        if( !Singleton.getInstance().getServer_Uid_me().equals(wsMsg_single.getSendid())){
                                            Log.d("TEST", "In For");
                                            for (SingleContact singleContact_ : user.getSingleContactList()) {
                                                if (singleContact_.getIsGroup() == 1 && wsMsg_single.getType().equals("broadcast")) {
                                                    if (singleContact_.getG_number().equals(wsMsg_single.getRecid())) {
                                                        singleContact = singleContact_;
                                                        S_id = singleContact_.getId();
                                                    }
                                                } else if (singleContact_.getIsGroup() == 0 && wsMsg_single.getType().equals("single")) {
                                                    if ((singleContact_.getM_id().equals(wsMsg_single.getSendid()))) {
                                                        singleContact = singleContact_;
                                                        S_id = singleContact_.getId();
                                                    }
                                                }
                                            }
                                            System.out.println(S_id);
                                            Bubble bubble = new Bubble();
                                            bubble.setContent(wsMsg_single.getContent());
                                            bubble.setImage(wsMsg_single.getImage());
                                            bubble.setTime(wsMsg_single.getPublishedTime());
                                            bubble.setType(Bubble.TYPE_RECEIVED);
                                            bubble.setAvatar(wsMsg_single.getAvatar());
                                            if (singleContact.getIsGroup() == 1) {
                                                bubble.setOwner(wsMsg_single.getName());
                                            }
                                            bubble.setSinglecontact_id(S_id);
                                            bubble.save();
                                            Intent intent1 = new Intent();
                                            intent1.setAction("com.ruichatver1_0.content");
                                            sendBroadcast(intent1);
                                        }
                                    }

                                }
                            });
                            Singleton.getInstance().getClient().addHeader("Cookie" , Singleton.getInstance().getHeader());
                            try {
                                Singleton.getInstance().getClient().connectBlocking();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            List<User> users = LitePal.where("Uid =?", Singleton.getInstance().getServer_Uid_me()).find(User.class);
                            if(users.size() == 0){
                                User user = new User();
                                user.setEmail(tempemail);
                                user.setPassword(temppass);
                                user.setUid(Singleton.getInstance().getServer_Uid_me());
                                user.save();
                            }
                            ///////////////////////////


                            OkHttpClient okHttpClient = new OkHttpClient();
                            Request request = new Request.Builder()
                                    .url("http" + Singleton.getInstance().getUrl() + "/api/user/friends")
                                    .addHeader("Cookie",Singleton.getInstance().getHeader())
                                    .get()
                                    .build();
                            okHttpClient.newCall(request).enqueue(new Callback() {
                                @Override
                                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                                    //Local_init();
                                    //initFriends();
                                    //Log.d("TEST","OnFailure");
                                }

                                @Override
                                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                                    String tempresponse =
                                            response.body().string();
                                    Gson gson = new Gson();
                                    GetFriendsResponse getFriendsResponse = gson.fromJson(tempresponse, GetFriendsResponse.class);
                                    List<String> friendIDs = getFriendsResponse.getData().getFriends();
                                    for(final String singleID : friendIDs){
                                        OkHttpClient okHttpClient = new OkHttpClient();
                                        Request request = new Request.Builder()
                                                .addHeader("Cookie", Singleton.getInstance().getServer_Uid_me())
                                                .url("http" + Singleton.getInstance().getUrl() + "/api/user/id/" + singleID)
                                                .get()
                                                .build();
                                        okHttpClient.newCall(request).enqueue(new Callback() {
                                            @Override
                                            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                                            }

                                            @Override
                                            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                                                String tempresponse = response.body().string();
                                                Gson gson = new Gson();
                                                GetAFriendResponse getAFriendResponse = gson.fromJson(tempresponse, GetAFriendResponse.class);
                                                User tempUser = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                                                int user_id = tempUser.getId();
                                                List<SingleContact> tempUserFriends = tempUser.getSingleContactList();
                                                int exist = 0;
                                                for(SingleContact singleContact : tempUserFriends) {
                                                    if (singleID.equals(singleContact.getM_id())) {
                                                        ++exist;
                                                        /*singleContact.setHead(getAFriendResponse.getMessage().getAvatar());
                                                        singleContact.setName(getAFriendResponse.getMessage().getName());
                                                        singleContact.setEmail(getAFriendResponse.getMessage().getEmail());
                                                        singleContact.setSex(getAFriendResponse.getMessage().getGender() == 1 ? "男" : "女");
                                                        singleContact.setMood(getAFriendResponse.getMessage().getBio());
                                                        singleContact.update(singleContact.getId());*/
                                                        break;
                                                    }
                                                }
                                                if(exist == 0){
                                                    SingleContact singleContact = new SingleContact();
                                                    singleContact.setM_id(singleID);
                                                    singleContact.setHead(getAFriendResponse.getMessage().getAvatar());
                                                    singleContact.setSex(getAFriendResponse.getMessage().getGender() == 1 ? "男" : "女");
                                                    singleContact.setMood(getAFriendResponse.getMessage().getBio());
                                                    singleContact.setEmail(getAFriendResponse.getMessage().getEmail());
                                                    singleContact.setName(getAFriendResponse.getMessage().getName());
                                                    singleContact.setUser_id(user_id);
                                                    singleContact.save();
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                            Intent intent1 = new Intent(Log_in.this, friendList.class);
                            startActivity(intent1);
                            Log_in.this.finish();
                        }

                        else{
                            wrong = 1;
                        }
                    }
                });
                if(wrong == 1) {
                    Toast.makeText(Log_in.this, "账号密码错误", Toast.LENGTH_SHORT).show();
                    wrong = 0;
                }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Log_in.this, Registion.class);
                startActivity(intent1);
                Log_in.this.finish();
            }
        });

    }

    private void initContacts(){

    }
}
/*
    1. 聊天记录需要更新到双方的本地记录中
    2. 发布朋友圈需要更新给自己SingleContact的List<Post>
    3. 评论该如何写
    4. 在MyFriend中更新点击事件
 */