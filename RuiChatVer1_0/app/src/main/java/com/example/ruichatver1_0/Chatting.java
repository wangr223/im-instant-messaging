package com.example.ruichatver1_0;
////聊天界面需要一个返回按钮
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.litepal.LitePal;
import org.litepal.tablemanager.Connector;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Chatting extends AppCompatActivity {
    private static final int CHOOSE_PHOTO = 2;
    private List<Bubble> bubbleList = new ArrayList<>();
    private String imagePath;
    private EditText inputText;
    private Button sendBtn;
    private RecyclerView bubbleRV;
    private BubbleAdapter adapter;
    private List<SingleContact> contactList = new ArrayList<>();
    private SingleContact singleContact;
    private String FID;
    private String name;
    private Button send_img;
    private int is_G = 0;
    //private int goBack = 0;
    //ver 1.0

    private class ChatMessageReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            initBubbles();
            adapter = new BubbleAdapter(bubbleList);
            System.out.println(bubbleList.size());
            bubbleRV.setAdapter(adapter);
            bubbleRV.scrollToPosition(adapter.getItemCount()-1);
            adapter.notifyItemInserted(bubbleList.size()-1);
        }
    }

    private void doRegisterReceiver(){
        ChatMessageReceiver chatMessageReceiver  = new ChatMessageReceiver();
        IntentFilter filter = new IntentFilter("com.ruichatver1_0.content");
        registerReceiver(chatMessageReceiver,filter);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        Connector.getDatabase();
        Intent intent = new Intent();
        intent.putExtra("result", 1);
        Chatting.this.setResult(RESULT_OK,intent);
        Intent receiver_intent = getIntent();
        FID = receiver_intent.getStringExtra("FID");
        is_G = receiver_intent.getIntExtra("IsGroup",0);
        System.out.println("ID: " + FID + "Group: " + is_G);
        doRegisterReceiver();
        initBubbles();
        TextView chat_title = findViewById(R.id.title_text);
        name = singleContact.getName();
        chat_title.setText(name);

        ActionBar acbar = getSupportActionBar();
        if(acbar != null)
            acbar.hide();
        inputText = findViewById(R.id.input_text);
        send_img = findViewById(R.id.send_img);
        send_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission();
            }
        });
        sendBtn = findViewById(R.id.send);
        bubbleRV =  findViewById(R.id.msg_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        bubbleRV.setLayoutManager(layoutManager);
        adapter = new BubbleAdapter(bubbleList);
        bubbleRV.setAdapter(adapter);
        bubbleRV.scrollToPosition(adapter.getItemCount()-1);
        sendBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){ //send a new message
                String content = inputText.getText().toString();
                if(! "".equals(content) ){
                    User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                    Date date = new Date(System.currentTimeMillis());
                    String time =  simpleDateFormat.format(date);
                    Bubble bubble = new Bubble();
                    bubble.setContent(content);
                    bubble.setType(Bubble.TYPE_SENT);
                    bubble.setAvatar(user.getHead());
                    bubble.setTime(time);
                    bubble.setSinglecontact_id(singleContact.getId());
                    bubble.save();
                    bubbleList.add(bubble);
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("sendid",Singleton.getInstance().getServer_Uid_me());
                    jsonObject.addProperty("recid",FID);
                    jsonObject.addProperty("publishedTime",time);
                    jsonObject.addProperty("content",content);
                    jsonObject.addProperty("name",user.getName());
                    Gson gson = new Gson();
                    if(is_G == 0) {
                        jsonObject.addProperty("type", "single");
                        String temp = gson.toJson(jsonObject);
                        Singleton.getInstance().getClient().send(temp);
                    }
                    else {
                        jsonObject.addProperty("type", "broadcast");
                        String temp = gson.toJson(jsonObject);
                        Singleton.getInstance().getClient().send(temp);
                    }

                    adapter.notifyItemInserted(bubbleList.size()-1);
                    bubbleRV.scrollToPosition(bubbleList.size()-1);
                    inputText.setText("");
                }
            }
        });

    }




    private void initBubbles(){/////////////在这里使用数据库初始化数据
        contactList = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class).getSingleContactList();
        System.out.println(contactList.size());
        for (SingleContact someone : contactList){
            if ( is_G == 0 && someone.getIsGroup() == 0){
                if(someone.getM_id().equals(FID)){
                    bubbleList = someone.getMsg_list();
                    singleContact = someone;
                    break;
                }
            }
            else if(is_G == 1 && someone.getIsGroup() == 1) {
                if(someone.getG_number().equals(FID)){
                    bubbleList = someone.getMsg_list();
                    singleContact = someone;
                    break;
                }
            }
        }
    }

    public String bitmapToString(Bitmap bitmap){
        //将Bitmap转换成字符串
        String string=null;
        ByteArrayOutputStream bStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,bStream);
        byte[]bytes=bStream.toByteArray();
        string= Base64.encodeToString(bytes, Base64.DEFAULT);
        return string;
    }


    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }else {
            openAlbum();
        }
    }



    private void openAlbum(){
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent,CHOOSE_PHOTO); //打开相册
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 1:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    openAlbum();
                }else {
                    Toast.makeText(this,"你拒绝了该权限", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case CHOOSE_PHOTO:
                if(resultCode == RESULT_OK){
                    //判断手机系统版本号
                    if(Build.VERSION.SDK_INT>=19){
                        //4.4及以上系统使用这个方法处理图片
                        handleImageOnKitKat(data);
                    }
                }
                break;
            default:
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void handleImageOnKitKat(Intent data){
        Uri uri = data.getData();
        if(DocumentsContract.isDocumentUri(this,uri)){
            //如果是document类型的Uri，则通过document id处理
            String docId = DocumentsContract.getDocumentId(uri);
            if("com.android.providers.media.documents".equals(uri.getAuthority())){
                String id = docId.split(":")[1];  //解析出数字格式的id
                String selection = MediaStore.Images.Media._ID+"="+id;
                imagePath = getImagePath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,selection);
            }else if("com.android.providers.downloads.documents".equals(uri.getAuthority())){
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public downloads"),Long.valueOf(docId));
                imagePath = getImagePath(contentUri,null);
            }
        }else if("content".equalsIgnoreCase(uri.getScheme())){
            //如果是file类型的Uri，直接获取图片路径即可
            imagePath = getImagePath(uri,null);
        }else if("file".equalsIgnoreCase(uri.getScheme())){
            //如果是file类型的Uri，直接获取图片路径即可
            imagePath = uri.getPath();
        }
        displayImage(imagePath); //根据图片路径显示图片
    }

    private String getImagePath(Uri uri,String selection){
        String path = null;
        //通过Uri和selection来获取真实的图片路径
        Cursor cursor = getContentResolver().query(uri,null,selection,null,null);
        if(cursor!= null){
            if(cursor.moveToFirst()){
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }

    private void displayImage(String imagePath){
        if(imagePath!=null && !imagePath.equals("")){
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            /*set_head.setVisibility(View.VISIBLE);
            set_head.setImageBitmap(bitmap);
            head_bitmap = bitmap;
            head_change = 1;*/

            User user = LitePal.where("Uid = ?", Singleton.getInstance().getServer_Uid_me()).findFirst(User.class);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
            Date date = new Date(System.currentTimeMillis());
            String time =  simpleDateFormat.format(date);
            Bubble bubble = new Bubble();
            bubble.setType(Bubble.TYPE_SENT);
            bubble.setAvatar(user.getHead());
            bubble.setImage(bitmapToString(bitmap));
            bubble.setTime(time);
            bubble.setSinglecontact_id(singleContact.getId());
            bubble.save();
            bubbleList.add(bubble);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("sendid",Singleton.getInstance().getServer_Uid_me());
            jsonObject.addProperty("recid",FID);
            jsonObject.addProperty("publishedTime",time);
            jsonObject.addProperty("image", bitmapToString(bitmap));
            jsonObject.addProperty("name",user.getName());
            Gson gson = new Gson();
            if(is_G == 0) {
                jsonObject.addProperty("type", "single");
                String temp = gson.toJson(jsonObject);
                Singleton.getInstance().getClient().send(temp);
            }
            else {
                jsonObject.addProperty("type", "broadcast");
                String temp = gson.toJson(jsonObject);
                Singleton.getInstance().getClient().send(temp);
            }

            adapter.notifyItemInserted(bubbleList.size()-1);
            bubbleRV.scrollToPosition(bubbleList.size()-1);

            //存储上次选择的图片路径，用以再次打开app设置图片
            SharedPreferences sp = getSharedPreferences("sp_img",MODE_PRIVATE);  //创建xml文件存储数据，name:创建的xml文件名
            SharedPreferences.Editor editor = sp.edit(); //获取edit()
            editor.putString("imgPath",imagePath);
            editor.apply();
        }else {
            Toast.makeText(this,"获取图片失败",Toast.LENGTH_SHORT).show();
        }
    }
}