package com.example.ruichatver1_0;

public class AFriend {
    private String frd_name;
    private String frd_head;
    private String f_ID;
    private char frd_topChar;


    public AFriend(String head, String name, char frd_topChar) {
        this.frd_head = head;
        this.frd_name = name;
        this.frd_topChar = frd_topChar;
    }

    public String getFrd_head() {
        return frd_head;
    }

    public void setFrd_head(String frd_head) {
        this.frd_head = frd_head;
    }

    public String getname() {
        return frd_name;
    }

    public char getFrd_topChar(){
        return frd_topChar;
    }

    public String getF_ID() {
        return f_ID;
    }

    public void setF_ID(String f_ID) {
        this.f_ID = f_ID;
    }
}